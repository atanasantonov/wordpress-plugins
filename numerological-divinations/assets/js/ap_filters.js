// filter dropdowns behavior
jQuery('body').on('change', '.ap-filter', function(){
    
    var post = jQuery.post( vars.url, {
            post_type: jQuery(this).attr('id'),
            parent: jQuery("option:selected", this).val()
        }
    );
    
    post.done(function(response) {        
        jQuery.each(JSON.parse(response), function(key,value){            
            jQuery("#"+key).empty().append(value);
        });
      });
});
