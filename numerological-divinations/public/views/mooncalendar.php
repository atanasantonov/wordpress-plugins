<article id="post-<?php the_ID(); ?>">
    <header class="entry-header">
        <div class="entry-meta">
            <div class="icons">
                <img class="moon-calendar-icon" src="<?php echo NUMEROLOGY_DIVINATION_ASSETS; ?>img/moon-phases/<?php echo get_post_meta(get_the_ID(), 'moon-phase', 'single'); ?>.svg" alt="<?php echo $moon_phase; ?>" title="<?php echo $moon_phase; ?>" />
                <img class="moon-calendar-icon" src="<?php echo NUMEROLOGY_DIVINATION_ASSETS; ?>img/zodiac/<?php echo get_post_meta(get_the_ID(), 'zodiac-sign', 'single'); ?>.svg" alt="<?php echo $zodiac_sign; ?>" title="<?php echo $zodiac_sign; ?>" />
            </div>
            <time class="entry-date">
                <a href="<?php the_permalink(); ?>">
                    <?php echo $date; ?>
                </a>
            </time>
        </div><!-- .entry-meta -->        
    </header><!-- .entry-header -->
    
    <h2 class="entry-title"></h2>
    
    <div class="entry-summary">
        
    <?php if ( has_post_thumbnail() && $show_thumbnail ) : ?>            
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
    <?php endif; ?>

    <?php the_excerpt(); ?>
        
    </div><!-- .entry-summary -->
</article><!-- #post-## --><hr>