<article id="post-<?php the_ID(); ?>">
    <header class="entry-header">
        <div class="entry-meta">
            <time class="entry-date">
                <a href="<?php the_permalink(); ?>">
                    <?php echo $date; ?>
                </a>
            </time>
        </div><!-- .entry-meta -->        
    </header><!-- .entry-header -->

    <h2 class="entry-title">
        <a href="<?php the_permalink(); ?>" rel="bookmark">
            <?php echo trim(substr(get_the_title(), 5)); ?>
        </a>
    </h2>
    
    <div class="entry-summary">
    <?php if ( has_post_thumbnail() && $show_thumbnail ) : ?>            
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
    <?php endif; ?>

    <?php if( $show_excerpt ) : ?>
    <?php the_excerpt(); ?>
    <?php endif; ?>
    </div><!-- .entry-summary -->
</article><!-- #post-## --><hr>