<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

if( !class_exists('Еlimarinova') ):
    
class Еlimarinova {
	
    private static $initiated = false;
    
    private static $class = 'Еlimarinova';
    private static $prefix = 'numerological-divinations_';
    public static  $textdomain  = 'numerological-divinations';
    private static $alphabet;
    
    public static  $lang;

    public static function init() {
        if ( !self::$initiated ) {
                self::init_hooks();
        }
    }

    /**
     * Initializes WordPress hooks
     */
    private static function init_hooks() {
        self::$initiated = true;
        
        wp_enqueue_style( 'numerological-divinations', NUMEROLOGY_DIVINATION_ASSETS.'css/numerological-divinations.css' );
        
        // register query vars
        add_filter( 'query_vars', array('Еlimarinova', 'numerological-divinations_add_query_vars' ));
        
        // sanovnik
        self::$alphabet = __( 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z', self::$textdomain );        
        add_shortcode('numerological-divinations_sanovnik', array( self::$class, self::$prefix.'sanovnik' ));
        add_shortcode('numerological-divinations_folklor', array( self::$class, self::$prefix.'folklor' ));
        add_shortcode('numerological-divinations_mooncalendar', array( self::$class, self::$prefix.'mooncalendar' ));
    }
    
    public static function numerological-divinations_add_query_vars($vars) {
        $vars[] = 'dream';
        return $vars;
    }
    
    public static function numerological-divinations_sanovnik() {
        
        global $wpdb;
        
        // common vars
        $the_query  = FALSE;
        $error      = '';
        $dream      = trim(get_query_var("dream"));
        
        if(isset($_POST['dream'])) :
            // check dream
            if($dream==='') :
                $error = __('Полето не може да бъде празно', 'numerological-divinations');
            elseif(mb_strlen($dream)>0 && mb_strlen($dream)<3) :
                $error = __('Думата трябва да съдържа поне три букви', 'numerological-divinations');
            else :
                // quering posts
                add_action('posts_where', array( self::$class, self::$prefix.'description_by_dream' ));

                $args = array( 
                    'post_type' => 'eli-sanovnik',
                    'posts_per_page' => 500,
                    'orderby'   => 'post_title',
                    'order'   => 'asc'
                ); 

                $the_query = new WP_Query($args); 
            endif;
        endif;

        include_once 'views/sanovnik.php';        
        return;
    }
    
    public static function numerological-divinations_description_by_dream($where) {
        $dream = get_query_var("dream", "false");
        // at the moment qTranslateX syntax supported
        $where .= " AND (post_title LIKE '".$dream."%' OR post_title LIKE '%[:".self::$lang."]".$dream."%')";        
        return $where;        
    }
    
    public static function numerological-divinations_folklor_by_month($where) { 
        $where  = " AND post_type = 'eli-folklor'";
        $where .= " AND post_status = 'publish'";
        $where .= " AND post_date BETWEEN '2006-".date("m")."-01 00:00:00' AND '2006-".date("m")."-31 23:59:59'";
        return $where;        
    }
    
    public static function numerological-divinations_folklor() { 
        
        echo '<br><br>';    
        
        global $wpdb;

        add_action('posts_where', array( 'Еlimarinova', 'numerological-divinations_folklor_by_month' ));

        // publish posts
        $query = new WP_Query( array( 
            'post_status'   => 'publish',
            'post_type'     => 'eli-folklor',
            'orderby'       => 'post_title',
            'order'         => 'asc',
            'posts_per_page'=>-1
        ));

        if($query->have_posts()) :
            while ($query->have_posts()) :
                $query->the_post();

                $date  = substr(get_the_date(), 0, 5) . '.' . date("Y");                 
                $date .= ', ' . __(date_format(date_create_from_format('d.m.Y', $date), 'l'), 'numerological-divinations'); 

                $show_thumbnail = TRUE;
                $show_excerpt = TRUE;

                include 'views/excerpt.php';

            endwhile;
        endif;  

        wp_reset_query(); 
        return TRUE;
    }
    
    public static function numerological-divinations_mooncalendar_by_month($where) { 
        $where  = " AND post_type = 'eli-lunen-calendar'";
        $where .= " AND post_status = 'publish'";
        $where .= " AND post_date BETWEEN '2006-".date("m")."-01 00:00:00' AND '2006-".date("m")."-31 23:59:59'";
        return $where;        
    }  
    
    public static function numerological-divinations_mooncalendar() {
        
        echo '<br><br>';
        
        global $wpdb, $moon_phases, $zodiac_signs;

        add_action('posts_where', array( 'Еlimarinova', 'numerological-divinations_mooncalendar_by_month' ));

        // publish posts
        $query = new WP_Query( array( 
            'post_status'   => 'publish',
            'post_type'     => 'eli-lunen-calendar',
            'orderby'       => 'post_title',
            'order'         => 'asc',
            'posts_per_page'=>-1
        ));

        if($query->have_posts()) :
            while ($query->have_posts()) :
                $query->the_post();

                $date  = substr(get_the_date(), 0, 5) . '.' . date("Y");                 
                $date .= ', ' . __(date_format(date_create_from_format('d.m.Y', $date), 'l'), 'numerological-divinations'); 
                
                $moon_phase = $moon_phases[get_post_meta(get_the_ID(), 'moon-phase', 'single')];
                $zodiac_sign = $zodiac_signs[get_post_meta( get_the_ID(), 'zodiac-sign', 'single' )];
                
                ?>

                <article id="post-<?php the_ID(); ?>" class="post moon-calendar">
                    <div class="entry-meta">
                        <div class="icons">
                            <img class="moon-calendar-icon" src="<?php echo NUMEROLOGY_DIVINATION_ASSETS; ?>img/moon-phases/<?php echo get_post_meta(get_the_ID(), 'moon-phase', 'single'); ?>.svg" alt="<?php echo $moon_phase; ?>" title="<?php echo $moon_phase; ?>" />
                            <img class="moon-calendar-icon" src="<?php echo NUMEROLOGY_DIVINATION_ASSETS; ?>img/zodiac/<?php echo get_post_meta(get_the_ID(), 'zodiac-sign', 'single'); ?>.svg" alt="<?php echo $zodiac_sign; ?>" title="<?php echo $zodiac_sign; ?>" />
                        </div>
                        <time class="entry-date">
                            <a href="<?php the_permalink(); ?>"><?php echo $date; ?></a>                                
                        </time><br>
                        <span class="small"><?php echo $moon_phase; ?><?php if($zodiac_sign!='') : echo ', '; endif;?><?php echo $zodiac_sign; ?></span>
                    </div>
    
                    <div class="entry-summary">
                    <?php if ( has_post_thumbnail() && $show_thumbnail ) : ?>            
                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                    <?php endif; ?>

                    <?php the_excerpt(); ?>
                    </div><!-- .entry-summary -->
                </article><hr><br>
<?php

            endwhile;
        endif;  

        wp_reset_query(); 
        return TRUE;
    }
}

endif;