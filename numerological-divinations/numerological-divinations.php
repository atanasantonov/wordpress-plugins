<?php
/**
 * @package Eli Marinova
 * @version 1.0
 */
/*
Plugin Name: Numerological Divinations
Plugin URI: http://nantstudio.eu/wordpress-plugins/numerological-divinations/
Description: Plugin for numerological divinations of Eli Marinova. Plugin calculates personal year, month and a day for the user and displays information about them. qTranslate Ready.
Author: Atanas Antonov
Text Domain: nantstudio
Domain Path: /languages 
Version: 1.0
Author URI: http://atanas.be/
*/

// Exit if called directly
if (!defined('WPINC')) { die; }

$plugin = 'NUMEROLOGICAL_DIVINATIONS';

if (!defined($plugin . '_PLUGIN_DIR')) 
{
    define($plugin . '_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

if (!defined($plugin . '_ASSETS')) 
{
    define($plugin . '_ASSETS', plugins_url('/assets/', __FILE__));
}


if (!defined($plugin . '_PLUGIN_VERSION')) 
{
    define( $plugin . '_PLUGIN_VERSION', '0.1' );
}

load_plugin_textdomain( 'numerological-divinations', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

// months
__('January', 'numerological-divinations') ;
__('February', 'numerological-divinations') ;
__('March', 'numerological-divinations') ;
__('April', 'numerological-divinations') ;
__('May', 'numerological-divinations') ;
__('June', 'numerological-divinations') ;
__('July', 'numerological-divinations') ;
__('August', 'numerological-divinations') ;
__('September', 'numerological-divinations') ;
__('October', 'numerological-divinations') ;
__('November', 'numerological-divinations') ;
__('December', 'numerological-divinations') ;

// weekdays
__('Monday', 'numerological-divinations') ;
__('Tuesday', 'numerological-divinations') ;
__('Wednesday', 'numerological-divinations') ;
__('Thursday', 'numerological-divinations') ;
__('Friday', 'numerological-divinations') ;
__('Saturday', 'numerological-divinations') ;
__('Sunday', 'numerological-divinations') ;

__('in', 'nantstudio');
__('for', 'nantstudio');

if (is_admin()) {
    
    add_action( 'wp_enqueue_scripts', 'numerological-divinations_admin_scripts' );
       
    require_once( NUMEROLOGICAL_DIVINATIONS_PLUGIN_DIR . 'admin/class-numerological-divinations-admin.php' );    
    add_action( 'init', array( 'Еlimarinova_Admin', 'init' ));    
    
    // register query vars
    add_filter( 'query_vars', array('NumerologicalDivinationsAdmin', 'add_query_vars' ));
    
//    // sanovnik
//    require_once 'admin/core/sanovnik.php';    
//    add_action( 'init', array( 'Еlimarinova_Admin_sanovnik', 'numerological-divinations_admin_sanovnik_init' ) );
//    
//    // folklor
//    require_once 'admin/core/folklor.php';
//    add_action( 'init', array( 'Еlimarinova_Admin_Folklor', 'numerological-divinations_admin_folklor_init' ) );
//    
//    // mooncalendar
//    require_once 'admin/core/lunen-calendar.php';
//    add_action( 'init', array( 'Еlimarinova_Admin_Mooncalendar', 'numerological-divinations_admin_mooncalendar_init' ) );    
//    add_action( 'save_post', array( 'Еlimarinova_Admin_Mooncalendar', 'numerological-divinations_admin_mooncalendar_save' ) );    
}
else 
{     
    require_once( NUMEROLOGY_DIVINATION_PLUGIN_DIR . 'public/class-numerological-divinations.php' );
    add_action( 'init', array( 'Еlimarinova', 'init' ) );
    
    // Show posts of 'post', 'page' and 'numerological-divinations-*' post types on home page
    add_action( 'pre_get_posts', 'add_my_post_types_to_query' );
    
    function add_my_post_types_to_query( $query ) {
        if (is_single() && $query->is_main_query() )
        {
            if(is_object($query))
            {
                $query->set( 'post_type', array( 'post', 'page', 'eli-folklor', 'eli-lunen-calendar' ) );
                return $query;
            }
        }            
    }
} 

// widgets
//require_once 'admin/widgets/banners-widget.php';
//require_once 'admin/widgets/folklor-widget.php';
//require_once 'admin/widgets/lunen-calendar-widget.php';