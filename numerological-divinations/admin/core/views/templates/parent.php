<select name="post_parent">
<?php foreach ($parents as $parent) : ?>
    <?php printf( '<option class="i18n-multilingual-display form-control" value="%s"%s>%s</option>', esc_attr( $parent->ID ), selected( $parent->ID, $post->post_parent, false ), esc_html( $parent->post_title ) ); ?>
<?php endforeach; ?>
</select>
