<hr>
<div class="row">
    <div class="col"> 
        <b><?php _e('Moon phase', 'numerological-divinations'); ?>:</b>
        <select name="moon-phase">
            <option></option>
            <?php foreach ($moon_phases as $key => $value) : ?>
            <?php $selected = (get_post_meta($post->ID, "moon-phase", true)==$key) ? ' selected="selected"' : '' ?>
            <option value="<?php echo $key; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="col"> 
        <b><?php _e('Zodiac sign', 'numerological-divinations'); ?>:</b>
        <select name="zodiac-sign">
            <option></option>
            <?php foreach ($zodiac_signs as $key => $value) : ?>
            <?php $selected = (get_post_meta($post->ID, "zodiac-sign", true)==$key) ? ' selected="selected"' : '' ?>
            <option value="<?php echo $key; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>
<hr>
<div class="row">
    <em>
    1. <?php _e('Moon day', 'numerological-divinations'); ?><br>
    2. <?php _e('Moon phase', 'numerological-divinations'); ?><br>
    3. <?php _e('Moon without course', 'numerological-divinations'); ?><br>
    4. <?php _e('Retrograde planet', 'numerological-divinations'); ?><br>
    5. <?php _e('Moon in sign', 'numerological-divinations'); ?><br>
    </em>
</div>