<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

class YearsAdmin extends NumerologicalDivinationsAdmin {
    
    private static $class      = 'YearsAdmin';   
    private static $post_type  = 'numerological-divinations-years';
    private static $parent     = FALSE;
    private static $slug       = 'years';
    private static $prefix     = '';
   
    // add sanovnik
    public static function years_init() {
        register_post_type( $post_type,
            array(
                'labels'        => array(
                    'name'            => __( 'Moon calendar', 'elimarinova' ),
                    'singular_name'   => __( 'Moon calendar', 'elimarinova' ),
                    'add_new'         => __( 'Add moon calendar', 'elimarinova' ),
                    'new_item'        => __( 'New moon calendar', 'elimarinova' ),
                    'add_new_item'    => __( 'Add new moon calendar', 'elimarinova' ),
                    'edit_item'       => __( 'Edit moon calendar', 'elimarinova' ),
                ),
                'public'        => true,
                'has_archive'   => false,
                'rewrite'       => array('slug' => self::$slug),
                'supports'      => array( 'title', 'editor' ),
                'capability_type'    => 'post',
                'show_in_menu'       => 'numerological-divinations',
                'publicly_queryable'  => true,
                'exclude_from_search' => false,
                'show_ui'             => true,
                'query_var'          => true,
                'taxonomies' => array('post_tag')
            )
        ); 
        
        add_action( 'edit_form_after_title', array( self::$class, 'elimarinova_admin_edit_form_after_title' ) );
    }
    
    public static function elimarinova_admin_edit_form_after_title($post) {
        
        if ( get_post_type($post) != self::$post_type ) return;
        
        global $moon_phases, $zodiac_signs;
        wp_nonce_field(basename(__FILE__), "nonce");
        include_once 'views/lunen-calendar.php';     
    }
    
    public static function elimarinova_admin_mooncalendar_save($post) { 
        
//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';
//        exit();
        
        if(!current_user_can("edit_post", $post))
        return $post;
        
        // check the nonce
        if (!isset($_POST["nonce"]) || !wp_verify_nonce($_POST["nonce"], basename(__FILE__)))
        return $post;
        
        if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post;
        
        // If this isn't a custom post, don't update it.
        if ( get_post_type($post) != self::$post_type ) return;
        
        if ( isset( $_POST['moon-phase'] ) ) {
            update_post_meta($post, "moon-phase", $_POST['moon-phase']);
        }
        
        if ( isset( $_POST['zodiac-sign'] ) ) {
            update_post_meta($post, "zodiac-sign", $_POST['zodiac-sign']);
        }     
    }
    
}

