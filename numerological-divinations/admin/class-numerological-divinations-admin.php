<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

if( !class_exists('NumerologicalDivinationsAdmin') ):

class NumerologicalDivinationsAdmin {
	
    private static  $initiated   = false;
    public static   $lang;
    public static   $textdomain  = 'numerological-divinations';

    public static function init() {
        
        if ( !self::$initiated ) {
                self::init_hooks();
        }
    }

    /**
     * Initializes WordPress hooks
     */
    private static function init_hooks() { 
        self::$lang = function_exists('qtranxf_getLanguage') ? qtranxf_getLanguage() : substr(get_locale(), 0, 2);
        
        add_action( 'admin_menu', array( 'NumerologicalDivinationsAdmin', 'admin_menu' )); 
        add_action( 'admin_init', array( 'NumerologicalDivinationsAdmin', 'admin_init' ) );
        
        // register query vars
        add_filter( 'query_vars', array('NumerologicalDivinationsAdmin', 'add_query_vars' ));
    }

    public static function admin_init() {   

        wp_enqueue_style( 'numerological-divinations', NUMEROLOGICAL_DIVINATIONS_ASSETS.'css/numerological-divinations-admin.css', array());
        
        // list the phrases for the pomo parsers
        __( 'Add', 'numerological-divinations' );
        __( 'Add new %s', 'numerological-divinations' );
        __( 'New %s', 'numerological-divinations' );
        __( 'Edit %s', 'numerological-divinations' );
        __( 'See %s', 'numerological-divinations' );
        __( '%s', 'numerological-divinations' ); //All
        __( 'Search %s', 'numerological-divinations' );
        __( 'Belongs to', 'numerological-divinations' );
        __( '%s not found', 'numerological-divinations' );
        __( '%s not found in trash.', 'numerological-divinations' );
    }
    
    public static function admin_menu() {
        add_menu_page(__('Dashboard', 'numerological-divinations'), __('Numerological Divinations', 'numerological-divinations'), 'edit_posts', 'numerological-divinations', array('NumerologicalDivinationsAdmin','admin_dashboard'),'dashicons-book');
    }
    
    public static function admin_dashboard () {       
        return;
    }
        
    public static function make_labels($singular, $plural)
    {       
        return array(
		'name'               => __( $plural, 'post type general name', 'numerological-divinations' ),
		'singular_name'      => __( $singular, 'post type singular name', 'numerological-divinations' ),
		'menu_name'          => __( $plural, 'admin menu', 'numerological-divinations' ),
		'name_admin_bar'     => __( $singular, 'add new on admin bar', 'numerological-divinations' ),
		'add_new'            => __( 'Add new', mb_strtolower($singular), 'numerological-divinations' ),
		'add_new_item'       => sprintf( __( 'Add new %s', 'numerological-divinations' ), $singular_trans),
		'new_item'           => sprintf( __( 'New %s', 'numerological-divinations' ), $singular_trans),
		'edit_item'          => sprintf( __( 'Edit %s', 'numerological-divinations' ), $singular_trans),
		'view_item'          => sprintf( __( 'See %s', 'numerological-divinations' ),$singular_trans),
		'all_items'          => sprintf( __( '%s', 'numerological-divinations' ), $plural_trans),
		'search_items'       => sprintf( __( 'Search %s', 'numerological-divinations' ), $plural_trans),
		'parent_item_colon'  => __( 'Belongs to ', 'numerological-divinations' ),
		'not_found'          => sprintf( __( '%s not found', 'numerological-divinations' ), mb_strtolower($plural_trans)),
		'not_found_in_trash' => sprintf( __( '%s not found in trash.', 'numerological-divinations' ), mb_strtolower($plural_trans))
	);
    }    
    
    public static function add_query_vars($vars) {
//        $vars[] = 'sanovnik_letter';
//        return $vars;
    }    
}

endif;