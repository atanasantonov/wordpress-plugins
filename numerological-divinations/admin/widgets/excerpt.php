<article id="post-<?php the_ID(); ?>" class="post">
    <header class="entry-header">
        <div class="entry-meta">
            <time class="entry-date">
                <a href="<?php the_permalink(); ?>">
                    <?php echo $date; ?>
                </a>
            </time>
        </div><!-- .entry-meta -->
        <h2 class="entry-title">
            <a href="<?php the_permalink(); ?>" rel="bookmark">
                <?php echo $title; ?>
            </a>
        </h2>
    </header><!-- .entry-header -->
    <div class="entry-summary">
    <?php if ( has_post_thumbnail() && $show_thumbnail ) : ?>            
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
    <?php endif; ?>

    <?php if( $show_excerpt ) : ?>
    <?php the_excerpt(); ?>
    <?php else : ?>
        <p class="link-more"></p>
    <?php endif; ?>
    </div><!-- .entry-summary -->
</article><!-- #post-## -->