<?php

class Еlimarinova_Banners_Widget extends WP_Widget {

    function __construct() {
            // Instantiate the parent object
            parent::__construct('', __('Sidebar Banners', 'numerological-divinations'), ''); 
    }

    function widget( $args, $instance ) {

        echo $args['before_widget'];
        echo $args['before_title'] . __('', 'numerological-divinations') .  $args['after_title'];
        
        $banners = array(
            1 => 'eli_marinova.swf'
        );
        
        $rand = rand( 1, sizeof($banners)); 
        
        $uploads = wp_get_upload_dir();
        
        $banner = $uploads['baseurl'] .'/banners/'. $banners[$rand];
        
        include_once 'banners.php';

        return TRUE;
    }

    function update( $new_instance, $old_instance ) {
            // Save widget options
    }

    function form( $instance ) {
            // Output admin widget options form
    }
}



function numerological-divinations_register_banners_widget() {
    register_widget( 'Еlimarinova_Banners_Widget' );
}

add_action( 'widgets_init', 'numerological-divinations_register_banners_widget' );
