<?php

class Еlimarinova_Traditions_Widget extends WP_Widget {

    function __construct() {
            // Instantiate the parent object
            parent::__construct('', __('Folklor Traditions', 'numerological-divinations'), ''); 
    }

    public static function numerological-divinations_traditions_by_month($where) {
        $where  = " AND post_type = 'eli-folklor'";
        $where .= " AND post_status = 'publish'";
        $where .= " AND post_date BETWEEN '2006-".date("m")."-".date("d")." 00:00:00' AND NOW()";

        return $where;        
    }
    
    public static function numerological-divinations_last_traditions($where) {
        $where  = " AND post_type = 'eli-folklor'";
        $where .= " AND post_status = 'publish'";
        
        return $where;        
    }   

    function widget( $args, $instance ) {

        echo $args['before_widget'];
        echo $args['before_title'] . __('Folklor Traditions', 'numerological-divinations') .  $args['after_title'];


        global $wpdb;

        add_action('posts_where', array( 'Еlimarinova_Traditions_Widget', 'numerological-divinations_traditions_by_month' ));

        // publish posts
        $query = new WP_Query( array( 
            'post_status'   => 'publish',
            'post_type'     => 'eli-folklor',
            'orderby'       => 'post_date',
            'order'         => 'asc',
            'posts_per_page'=>3
        ));

        if($query->have_posts()) :
            while ($query->have_posts()) :
                $query->the_post();

                $permalink = get_the_permalink();

                $date  = substr(get_the_date(), 0, 5) . '.' . date("Y");                 
                $date .= ', ' . __(date_format(date_create_from_format('d.m.Y', $date), 'l'), 'numerological-divinations'); 

                $title = trim(substr(get_the_title(), 5));

                $show_thumbnail = TRUE;
                $show_excerpt = TRUE;

                include 'excerpt.php';

            endwhile;
            wp_reset_query(); 
        else :

            add_action('posts_where', array( 'Еlimarinova_Traditions_Widget', 'numerological-divinations_remove_traditions_by_month' ));
            add_action('posts_where', array( 'Еlimarinova_Traditions_Widget', 'numerological-divinations_last_traditions' ));

            $query = new WP_Query( array( 
                'orderby' => array('date' => 'desc'),
                'posts_per_page'=>3
            ));
            
            while ($query->have_posts()) :
                $query->the_post();

                $permalink = get_the_permalink();

                $date  = substr(get_the_date(), 0, 5) . '.' . date("Y");                 
                $date .= ', ' . __(date_format(date_create_from_format('d.m.Y', $date), 'l'), 'numerological-divinations'); 

                $title = trim(substr(get_the_title(), 5));

                $show_thumbnail = TRUE;
                $show_excerpt = TRUE;

                include 'excerpt.php';

            endwhile;
            wp_reset_query(); 
        endif;  


        return TRUE;
    }

    function update( $new_instance, $old_instance ) {
            // Save widget options
    }

    function form( $instance ) {
            // Output admin widget options form
    }
        
    public static function numerological-divinations_remove_traditions_by_month() {
        remove_action( 'posts_where', array( 'Еlimarinova_Traditions_Widget', 'numerological-divinations_traditions_by_month' ));
    }
}



function numerological-divinations_register_folklor_widget() {
    register_widget( 'Еlimarinova_Traditions_Widget' );
}

add_action( 'widgets_init', 'numerological-divinations_register_folklor_widget' );
