<?php

class Еlimarinova_Mooncalendar_Widget extends WP_Widget {

	function __construct() {
		// Instantiate the parent object
		parent::__construct('mooncalendar', __('Mooncalendar', 'numerological-divinations'), ''); 
	}
        
        public static function numerological-divinations_mooncalendar_by_month($where) {  
            $where  = " AND post_type = 'eli-lunen-calendar'";
            $where .= " AND post_status = 'publish'";
            $where .= " AND post_date BETWEEN '2006-".date("m")."-".date("d")." 00:00:00' AND NOW()";
             
            return $where;        
        }
        
        public static function numerological-divinations_last_mooncalendar($where) {  
            $where  = " AND post_type = 'eli-lunen-calendar'";
            $where .= " AND post_status = 'publish'";
            
            return $where;        
        }  

	function widget( $args, $instance ) {

            echo $args['before_widget'];
            echo $args['before_title'] . __('Mooncalendar', 'numerological-divinations') .  $args['after_title'];
            

            global $wpdb, $moon_phases, $zodiac_signs;

            add_action('posts_where', array( 'Еlimarinova_Mooncalendar_Widget', 'numerological-divinations_mooncalendar_by_month' ));
          
            // publish posts
            $query = new WP_Query( array( 
                'post_status'   => 'publish',
                'post_type'     => 'eli-lunen-calendar',
                'orderby'       => 'post_date',
                'order'         => 'asc',
                'posts_per_page'=>3
            ));
            
            if($query->have_posts()) :
                while ($query->have_posts()) :
                    $query->the_post();

                    $date  = substr(get_the_date(), 0, 5) . '.' . date("Y");                 
                    $date .= ', ' . __(date_format(date_create_from_format('d.m.Y', $date), 'l'), 'numerological-divinations'); 
                    $moon_phase = $moon_phases[get_post_meta(get_the_ID(), 'moon-phase', 'single')];
                    $zodiac_sign = $zodiac_signs[get_post_meta( get_the_ID(), 'zodiac-sign', 'single' )];
                    ?>
            
                <article id="post-<?php the_ID(); ?>" class="post moon-calendar">
                    <header class="entry-header">
                        <div class="entry-meta">
                            <div class="icons">
                                <img class="moon-calendar-icon" src="<?php echo NUMEROLOGY_DIVINATION_ASSETS; ?>img/moon-phases/<?php echo get_post_meta(get_the_ID(), 'moon-phase', 'single'); ?>.svg" alt="<?php echo $moon_phase; ?>" title="<?php echo $moon_phase; ?>" />
                                <img class="moon-calendar-icon" src="<?php echo NUMEROLOGY_DIVINATION_ASSETS; ?>img/zodiac/<?php echo get_post_meta(get_the_ID(), 'zodiac-sign', 'single'); ?>.svg" alt="<?php echo $zodiac_sign; ?>" title="<?php echo $zodiac_sign; ?>" />
                            </div>
                            <time class="entry-date">
                                <a href="<?php the_permalink(); ?>"><?php echo $date; ?></a>                                
                            </time><br>
                            <span class="small"><?php echo $moon_phase; ?><?php if($zodiac_sign!='') : echo ', '; endif;?><?php echo $zodiac_sign; ?></span>
                        </div>
                    </header>
                </article>        
            
            
            <?php
            
                endwhile;
           
            else :
              
                
                add_action('posts_where', array( 'Еlimarinova_Mooncalendar_Widget', 'numerological-divinations_remove_mooncalendar_by_month' ));
                add_action('posts_where', array( 'Еlimarinova_Mooncalendar_Widget', 'numerological-divinations_last_mooncalendar' ));
                
                // publish posts
                wp_reset_query();
                $query = new WP_Query( array( 
                    'post_status'   => 'publish',
                    'post_type'     => 'eli-lunen-calendar',
                    'orderby'       => 'post_date',
                    'order'         => 'asc',
                    'posts_per_page'=>3
                ));

                if($query->have_posts()) :
                    while ($query->have_posts()) :
                        $query->the_post();

                        $date  = substr(get_the_date(), 0, 5) . '.' . date("Y");                 
                        $date .= ', ' . __(date_format(date_create_from_format('d.m.Y', $date), 'l'), 'numerological-divinations');

                        $moon_phase = $moon_phases[get_post_meta(get_the_ID(), 'moon-phase', 'single')];
                        $zodiac_sign = $zodiac_signs[get_post_meta( get_the_ID(), 'zodiac-sign', 'single' )];
                        ?>

                    <article id="post-<?php the_ID(); ?>" class="post moon-calendar">
                        <header class="entry-header">
                            <div class="entry-meta">
                                <div class="icons">
                                    <img class="moon-calendar-icon" src="<?php echo NUMEROLOGY_DIVINATION_ASSETS; ?>img/moon-phases/<?php echo get_post_meta(get_the_ID(), 'moon-phase', 'single'); ?>.svg" alt="<?php echo $moon_phase; ?>" title="<?php echo $moon_phase; ?>" />
                                    <img class="moon-calendar-icon" src="<?php echo NUMEROLOGY_DIVINATION_ASSETS; ?>img/zodiac/<?php echo get_post_meta(get_the_ID(), 'zodiac-sign', 'single'); ?>.svg" alt="<?php echo $zodiac_sign; ?>" title="<?php echo $zodiac_sign; ?>" />
                                </div>
                                <time class="entry-date">
                                    <a href="<?php the_permalink(); ?>"><?php echo $date; ?></a>                                
                                </time><br>
                                <span class="small"><?php echo $moon_phase; ?><?php if($zodiac_sign!='') : echo ', '; endif;?><?php echo $zodiac_sign; ?></span>
                            </div>
                        </header>
                    </article>        


                <?php

                    endwhile;
                endif;
            endif;            
            wp_reset_query(); 
            echo  '<br>';

            return TRUE;
        }

	function update( $new_instance, $old_instance ) {
		// Save widget options
	}

	function form( $instance ) {
		// Output admin widget options form
	}
        
        public static function numerological-divinations_remove_mooncalendar_by_month() {
            remove_action( 'posts_where', array( 'Еlimarinova_Mooncalendar_Widget', 'numerological-divinations_mooncalendar_by_month' ));
        }
}



function numerological-divinations_register_mooncalendar_widget() {
    register_widget( 'Еlimarinova_Mooncalendar_Widget' );
}

add_action( 'widgets_init', 'numerological-divinations_register_mooncalendar_widget' );
