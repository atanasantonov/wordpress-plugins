<?php defined( 'ABSPATH' ) or die( 'I can\'t do anything alone! Sorry!' ); ?>

<div class="wrap">
<h1>Airport Transfers</h1>
<p class="description"><span class="dashicons dashicons-admin-settings"></span> <?php echo __('Styling and functionality settings',self::$textdomain); ?>.</p>

<form method="post" action="options.php">
  <?php settings_fields( self::$settings ); ?>
  <?php do_settings_sections( self::$settings ); ?>
  
  <table class="form-table">  
    
    <tr>
      <th scope="row"><?php echo __('Admin email/emails',self::$textdomain); ?></th>
      <td>
        <input type="text" name="ns-airport-transfers-admin-email" value="<?php echo esc_attr( get_option('ns-airport-transfers-admin-email') ); ?>" class="large-text" />
        <p class="description"><?php echo __('Email/emails for delivery. Comma separated if more than one (admin1@example.com, admin2@example.com, ...)',self::$textdomain); ?></p>
      <br><hr></td>
    </tr>
    
    <tr>
      <th scope="row"><?php echo __('Contact email',self::$textdomain); ?></th>
      <td>
        <input type="text" name="ns-airport-transfers-contact-email" value="<?php echo esc_attr( get_option('ns-airport-transfers-contact-email') ); ?>" class="large-text" />
        <p class="description"><?php echo __('Contact email.',self::$textdomain); ?></p>
      <br><hr></td>
    </tr>
    
    <tr>
      <th scope="row"><?php echo __('Contact phone/phones',self::$textdomain); ?></th>
      <td>
        <input type="text" name="ns-airport-transfers-contact-phone" value="<?php echo esc_attr( get_option('ns-airport-transfers-contact-phone') ); ?>" class="large-text" />
        <p class="description"><?php echo __('Contact phone/phones.',self::$textdomain); ?></p>
      <br><hr></td>
    </tr>
        
    <tr>
      <th scope="row"><?php echo __('Default destination country',self::$textdomain); ?></th>
      <td>
        <input type="text" name="ns-airport-transfers-country" value="<?php echo esc_attr( get_option('ns-airport-transfers-country') ); ?>" />
        <p class="description"><?php echo __('Set the country (also can be set in the shortcode [airport-transfers-form country="Earthsea"])',self::$textdomain); ?></p>  
      </td>
    </tr>
    
    <tr>
      <th scope="row"><?php echo __('Resort type',self::$textdomain); ?></th>
      <td>
        <select name="ns-airport-transfers-resort">
        <?php $options = array(
              'summer' => 'Summer',
              'winter' => 'Winter'
          ); ?>
        <?php $format = esc_attr( get_option('ns-airport-transfers-resort')); ?>
        <?php foreach ($options as $value => $name) : ?>
          <?php $selected = ($format===$value) ? ' selected="selected"' : ''; ?>
          <option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo __($name,self::$textdomain); ?></option>
        <?php endforeach; ?>
        </select>
        <p class="description"><?php echo __('Select default resort type (also can be set in the shortcode [airport-transfers-form resort="summer/winter"])',self::$textdomain); ?></p>  
      </td>
    </tr>
    
    <tr>
      <th scope="row"><?php echo __('Time format',self::$textdomain); ?></th>
      <td>
        <select name="ns-airport-transfers-time-format">
        <?php $options = array('12','24'); ?>
        <?php $format = esc_attr( get_option('ns-airport-transfers-time-format')); ?>
        <?php foreach ($options as $option) : ?>
          <?php $selected = ($format===$option) ? ' selected="selected"' : ''; ?>
          <option value="<?php echo $option; ?>"<?php echo $selected; ?>><?php echo $option; ?> <?php echo __('Hours',self::$textdomain); ?></option>
        <?php endforeach; ?>
        </select>
        <p class="description"><?php echo __('Select default time-format (also can be set in the shortcode [airport-transfers-form time-format="24"])',self::$textdomain); ?></p>  
      </td>
    </tr>
  </table>
  <?php submit_button(); ?>
</form>
</div>