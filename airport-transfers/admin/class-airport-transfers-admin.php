<?php defined( 'ABSPATH' ) or die( 'I can\'t do anything alone! Sorry!' );

if( !class_exists('NS_Airport_Transfers_Admin') ):

class NS_Airport_Transfers_Admin {
	
  private static  $initiated   = FALSE;
  private static  $class       = 'NS_Airport_Transfers_Admin';
  public static   $prefix      = 'ns_admin_';
  public static   $textdomain  = 'ns-airport-transfers';
  public static   $settings    = 'ns-airport-transfers-group';

  public static function init() {        
    if(!self::$initiated) {
      self::ns_admin_init_hooks();
    }
  }

  /**
  * Initializes WordPress hooks
  */
  private static function ns_admin_init_hooks() { 
    add_action( 'admin_init', array( self::$class, self::$prefix.'init' ) );
    add_action( 'admin_init', array( self::$class, self::$prefix.'texts' ) );
    add_action( 'admin_menu', array( self::$class, self::$prefix.'menu' ) );    
    add_action( 'admin_init', array( self::$class, self::$prefix.'register_settings' ) );
    
    self::$initiated = TRUE;
  }

  public static function ns_admin_init() {

  }
  
  public static function ns_admin_menu() {
    add_menu_page('Airport Transfers', 'Airport Transfers', 'edit_posts', self::$textdomain.'-settings', array( self::$class, self::$prefix.'settings_page' ),'dashicons-admin-generic');
  }
  
  public static function ns_admin_register_settings() {
    register_setting( self::$settings, 'ns-airport-transfers-admin-email' );
    register_setting( self::$settings, 'ns-airport-transfers-contact-email' );
    register_setting( self::$settings, 'ns-airport-transfers-contact-phone' );
    register_setting( self::$settings, 'ns-airport-transfers-country' );
    register_setting( self::$settings, 'ns-airport-transfers-resort' );
    register_setting( self::$settings, 'ns-airport-transfers-time-format' );
  }
  
  public static function ns_admin_settings_page() {
    require_once( plugin_dir_path(__FILE__) . 'views/settings-page.php' );
  } 

  public static function ns_admin_texts() {

    // prepare the texts
    global $texts;

    foreach ($texts as $text) 
    {
      __($text,self::$textdomain);
      if(function_exists('pll_register_string')) 
      {
        pll_register_string(__('Form elements',self::$textdomain), $text, 'Airport Transfers');   
      }  
    }
  }
  
  // ajax form handler
  public static function ns_admin_send() {    
    try { 
      // check post at all
      if(empty($_POST)) {
        throw new Exception(airport_transfers_lang('Incorrect data!'));
      }
      
      $post = $_POST;

      
      $lang = sanitize_text_field($post['lang']);
      
      // check email
      $email = filter_var(sanitize_email($post['email']), FILTER_VALIDATE_EMAIL);
      if (!$email) {
        throw new Exception(airport_transfers_lang('Invalid email address!'));
      }
      
      // Subject
      $subject  = airport_transfers_lang('Request');

      // Message      
      $message = '';
      
      // information
      $message .= airport_transfers_lang('Your Name', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['name'])).PHP_EOL;
      $message .= airport_transfers_lang('Email Address', $lang).": ".$email.PHP_EOL;
      $message .= airport_transfers_lang('Mobile', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['phone'])).PHP_EOL;
      $message .= airport_transfers_lang('I would like to', $lang).": ".sanitize_text_field($post['like_to']).PHP_EOL;
      $message .= airport_transfers_lang('Transfer type', $lang).": ".airport_transfers_lang(sanitize_text_field($post['transfer_type']), $lang).PHP_EOL;
      $message .= airport_transfers_lang('No. of adults', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['adults'])).PHP_EOL;
      $message .= airport_transfers_lang('No. of children', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['children'])).PHP_EOL;
      $message .= airport_transfers_lang('No. of koffers', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['koffers'])).PHP_EOL; 
      if(sanitize_text_field($post['resort'])==='winter') {
        $message .= airport_transfers_lang('No. of ski/snowboards', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['ski'])).PHP_EOL;
      }
 
      $message .= PHP_EOL;
      
      // transfer_type
      $transfer_type = sanitize_text_field($post['transfer_type']);
      
      // time format
      $time_format = sanitize_text_field($post['time_format']);
      
      // arrival  
      $post_arrival_us_time = sanitize_text_field($post['arrival_us_time']);
      $arrival_us_time = ($time_format==='12'&&$post_arrival_us_time!=='') ? $post_arrival_us_time : '';
      if($transfer_type==='two_way' || $transfer_type==='from_airport') {
        $message .= airport_transfers_lang('Date of arrival', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['arrival_date'])).PHP_EOL;       
        $message .= airport_transfers_lang('Time of arrival', $lang).": ".sanitize_text_field($post['arrival_hour']).':'.sanitize_text_field($post['arrival_minutes']).$arrival_us_time.PHP_EOL;        
        $message .= airport_transfers_lang('Airport', $lang).": ".sanitize_text_field($post['arrival_airport']).PHP_EOL;
        $message .= airport_transfers_lang('Flight number', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['arrival_flight'])).PHP_EOL;
        $message .= PHP_EOL;
      }
      
      // departure
      $post_out_us_time = sanitize_text_field($post['out_us_time']);
      $out_us_time = ($time_format==='12'&&$post_out_us_time!=='') ? $post_out_us_time : '';
      if($transfer_type==='two_way' || $transfer_type==='to_airport') {
        $message .= airport_transfers_lang('Date of departure', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['out_date'])).PHP_EOL;        
        $message .= airport_transfers_lang('Time of departure', $lang).": ".sanitize_text_field($post['out_hour']).':'.sanitize_text_field($post['out_minutes']).$out_us_time.PHP_EOL;        
        $message .= airport_transfers_lang('Airport', $lang).": ".sanitize_text_field($post['out_airport']).PHP_EOL;
        $message .= airport_transfers_lang('Flight number', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['out_flight'])).PHP_EOL;
        $message .= PHP_EOL;
      }
      
      // address in destination country
      $message .= airport_transfers_lang('Address in', $lang).' '.airport_transfers_lang(get_option('ns-airport-transfers-country'), $lang).PHP_EOL;
      $message .= airport_transfers_lang('Address of hotel/accommodation', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['adress'])).PHP_EOL;
      
      // instructions
      $message .= airport_transfers_lang('Remarks/instructions', $lang).": ".airport_transfers_check_val(sanitize_text_field($post['instructions'])).PHP_EOL;
      
      // Headers
      $headers  = 'MIME-Version: 1.0' . PHP_EOL;
      $headers .= 'Content-type: text/plain; charset=utf-8' . PHP_EOL;
      $headers .= 'Content-Transfer-Encoding: 7bit' . PHP_EOL;
      $headers .= 'Message-ID: <' . time() . '-'. md5($email).'>' . PHP_EOL;
      $headers .= 'From: '.get_option('blogname').' <'.get_option('admin_email').'>' . PHP_EOL;            
      $headers .= 'Return-Path: <'.get_option('admin_email').'>' . PHP_EOL;
      
      // get administrative emails for the form
      if(!mail(get_option('ns-airport-transfers-admin-email'), $subject, $message, $headers)) {
        $error_message  = 'Sending mail failed!'.PHP_EOL.PHP_EOL;
        $error_message  = 'Form message:'.PHP_EOL.PHP_EOL;
        $error_message .= print_r($message,TRUE).PHP_EOL;          
        mail(get_option('admin_email'), 'Airport Transfers Plugin', $error_message, $headers);
        throw new Exception(airport_transfers_lang('System Error!', $lang));
      }
      
      $result['status'] = 'success';
      $result['data'] = airport_transfers_lang('Thank you for your submission! We will answer as soon as possible!', $lang); 
      
    } catch (Exception $e) {
      $result['status'] = 'error';
      $result['data'] = $e->getMessage(); 
    } finally {
      echo json_encode($result);      
    }
    wp_die();
  }
}

endif;