<?php 
/*
Plugin Name: Airport Transfers
Plugin URI:  https://github.com/atanasantonov/wordpress-plugins/tree/master/airport-transfers
Description: Plugin delivers multilingual form for airport transfers
Version:     1.0
Author:      Atanas Antonov
Author URI:  http://nantstudio.eu/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: ns-airport-transfers
Domain Path: /languages
*/

defined( 'ABSPATH' ) or die( 'I can\'t do anything alone! Sorry!' );
  
register_activation_hook( __FILE__, 'ns_airport_transfers_activate' );
register_deactivation_hook( __FILE__, 'ns_airport_transfers_uninstall' );

// add the options defaults
function ns_airport_transfers_activate() {
  add_option( 'ns-airport-transfers-admin-email', get_option('admin_email')); // Admin email delivery and contact address 
  add_option( 'ns-airport-transfers-contact-email', '' ); // Contant email
  add_option( 'ns-airport-transfers-contact-phone', '' ); // Contant phone
  add_option( 'ns-airport-transfers-country', 'Earthsea' ); // Can be set in plugin's settings page or trough the shortcode
  add_option( 'ns-airport-transfers-resort', 'summer' );  // Include ski field or not. Can be set in plugin's settings page or trough the shortcode
  add_option( 'ns-airport-transfers-time-format', '24' );  // 12 or 24 Hours time format. Can be set in plugin's settings page or trough the shortcode
}

// delete the unnecessary options from the database
function ns_airport_transfers_uninstall() { 
  delete_option('ns-airport-transfers-admin-email');
  delete_option('ns-airport-transfers-contact-email');
  delete_option('ns-airport-transfers-contact-phone');
  delete_option('ns-airport-transfers-country');
  delete_option('ns-airport-transfers-resort');
  delete_option('ns-airport-transfers-time-format');
}

load_plugin_textdomain('ns-airport-transfers', false, basename( dirname( __FILE__ ) ) . '/languages' );
require_once( plugin_dir_path(__FILE__) . 'texts.php' );

/**
 * airport_transfers_lang
 * 
 * returns translation of the string
 * @param string $string - string to be translated
 * @param string $lang - desired language
 * Function check if Polylang plugin's 'pll_translate_string' function
 * is available. If so the plugin's function will be used. Otherwise Wordpress translation system will be used. 
*/
function airport_transfers_lang($string='',$lang=null) {
  if(function_exists('pll_translate_string')) {
    $string = isset($lang) ? pll_translate_string($string, $lang) : pll__($string);
  } else {
    $string = __($string, 'ns-airport-transfers');
  }
  return $string;
}

/**
 * airport_transfers_lang_e
 * 
 * returns translation of the string with echo
 * @param string $string - string to be translated
 * @param string $lang - desired language
 * same as airport_transfers_lang but with echo
*/
function airport_transfers_lang_e($string='',$lang=null) {
  if(function_exists('pll_translate_string')) {
    $string = isset($lang) ? pll_translate_string($string, $lang) : pll__($string);
  } else {
    $string = __($string, 'ns-airport-transfers');
  }
  echo $string;
  return;
}

/**
 * airport_transfers_check_val
 * 
 * returns '-' if param is empty string
 * @param string $string
*/
function airport_transfers_check_val($string='') {
  $string = ($string!='') ? $string : '-';
  return $string;
}

if (is_admin()) {  
  require_once( plugin_dir_path(__FILE__) . 'admin/class-airport-transfers-admin.php' );
  add_action( 'init', array( 'NS_Airport_Transfers_Admin', 'init' ) );
  
  // register ajax functions for the fronend
  add_action( 'wp_ajax_send', array( 'NS_Airport_Transfers_Admin', 'ns_admin_send' ));
  add_action( 'wp_ajax_nopriv_send', array( 'NS_Airport_Transfers_Admin', 'ns_admin_send' ));
}
else 
{ 
  function ns_enqueue_scripts() {
    // load css
    wp_enqueue_style( 'bootstrap', plugins_url('assets/bootstrap/css/bootstrap.min.css', __FILE__));
    wp_enqueue_style( 'jquery-ui', plugins_url('assets/jquery-ui/jquery-ui.min.css', __FILE__));
    wp_enqueue_style( 'ns-airport-transfers', plugins_url('assets/css/styles.css', __FILE__));

    // load scripts
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-datepicker');        
    wp_enqueue_script('bootstrap', plugins_url('assets/bootstrap/js/bootstrap.min.js',  __FILE__ ),array('jquery'));
  }
  add_action( 'wp_enqueue_scripts','ns_enqueue_scripts');
  
  require_once( plugin_dir_path(__FILE__) . 'public/class-airport-transfers.php' );    
  add_action( 'init', array( 'NS_Airport_Transfers', 'init' ) );
} 
