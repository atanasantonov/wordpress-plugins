=== Airport Transfers ===

Contributors: atanasantonov

Donate link (BTC/XBT): 12xJuuJPr5VinbyKvueB5WLWnMxtz3ci8J

Tags: transfer, airport, arrival, departure

Requires at least: 3.0.1

Tested up to: 4.8

Requires PHP: 5.6

Stable tag: trunk

License: GPLv2

License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
Special airport transfers form that delivers mail to an administrator with customer's data.
 
== Description ==
 
Form is designed to serve as transfer order tool. Design is airport transfers ready.

The plugin has full l10n and i18n support. Polylang compatible!
 
There are several options like mails for the delivery and default settings.
  
== Installation ==

Download and activate the plugin. The plugin works with shortcode:

[airport-transfers-form] Airport 1, Airport 2, Airport 3 [/airport-transfers-form]

Airports must be comma separated and palced in between the shortcode tags. Just palce the shortcode and the airports in the desired language.

There is nice settings page where to set contact information and the default settings. You can also place some settings in the shortcode in every page.

Example: [airport-transfers-form country="Earthsea" time-format="12" resort="winter"] Paris Airport, Frankfurt Airport, Madrid Airport [/airport-transfers-form]

After the settings are done and the shortcode is placed on the desired pages you are ready to operate.
 
== Frequently Asked Questions ==

= Can I customize where the emails will be delivered? =
 
Yes, you can add as many emails for delivery as you want. The only rule is that they must be comma separated.

= Can I place a custom contact information? =
 
Yes, you can place your phone/phones and email.

= Can I customize the destination country? =
 
Yes, you can set any country you want. It can be a default one and as option in every page where the shorcode is placed.

= Can I switch between 12 and 24 hour format? =
 
Yes, you can choose default the format in the settings page and also as option in every page where the shorcode is placed..
 
= Is there a limitation of airports number? =
 
No, you can add as many airports in the dropdown as you wish.
 
= What about the number of request? =
 
There is no limitation. You can recieve as many orders as they are submitted.

= Is there a mandatory fields verification? =
 
Yes, the most important fields are validated twice - both on client's machine and on the server. Especially the email address.

= How can the customer understand that is missing something? =
 
There is a nice gentle modal window that is opened when mandatory field is empty.
 
== Screenshots ==
 
1. This screen shows the settings page (screenshot-1.jpg). 

2. This is the second screen shot (screenshot-2.jpg) shows the form in the page on frontend 
 
== Changelog ==
 
= 1.0 =
* First release.
* No changes.
 
== Upgrade Notice ==
 
= 1.0 =

No upgrades needed at this time.