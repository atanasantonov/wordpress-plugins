<?php defined( 'ABSPATH' ) or die( 'I can\'t do anything alone! Sorry!' ); ?>

<form id="ns-airport-transfer-form" action="<?=get_permalink()?>" class="form-horizontal" method="post" accept-charset="utf-8">
<noscript><?=airport_transfers_lang_e('Javascript must be enabled!');?></noscript><br><br>
<input type="hidden" id="resort" value="<?=$params['resort']?>">
<input type="hidden" id="time_format" value="<?=$params['time-format']?>">

  <h4 class="text-success"><?=airport_transfers_lang_e('Information about you');?></h4>

  <div class="form-group">
    <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Your name');?><span class="text-danger">*</span></label>
      <div class="col-sm-8 col-xs-12">
        <input type="text" id="name" class="form-control" value="" placeholder="">
      </div>
  </div>
  <div class="clear"><br></div>
  <div class="form-group">
      <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Email address');?><span class="text-danger">*</span></label>
      <div class="col-sm-8 col-xs-12">
        <input type="text" id="email" class="form-control" value="" placeholder="">
        <span class="explanation">(<?=airport_transfers_lang_e('On this Email you will recieve confirmation');?>)</span>
      </div>          
  </div>
  <div class="clear"></div>
  <div class="form-group">
      <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Mobile');?><span class="text-danger">*</span></label>
      <div class="col-sm-8 col-xs-12">
        <input type="text" id="phone" class="form-control" value="" placeholder="">
        <span class="explanation">(<?=airport_transfers_lang_e('It\'s mandatory to input your phone number');?>)</span>
      </div>          
  </div>
  <div class="clear"></div>
  <div class="form-group">
      <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('I would like to');?>:<span class="text-danger">*</span></label>
      <div class="col-sm-8 col-xs-12">
        <input type="radio" name="like_to" value="<?=airport_transfers_lang_e('Make reservation');?>" checked> <?=airport_transfers_lang_e('Make reservation');?><br>
        <input type="radio" name="like_to" value="<?=airport_transfers_lang_e('Request more information');?>"> <?=airport_transfers_lang_e('Request more information');?>
      </div> 
  </div><hr>
  <div class="clear"></div>
  <div class="form-group">
      <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Transfer type');?><span class="text-danger">*</span></label>
      <div class="col-sm-8 col-xs-12">
        <select id="transfer_type" class="form-control">
          <option value="two_way"><?=airport_transfers_lang_e('Two way');?></option>
          <option value="from_airport"><?=airport_transfers_lang_e('One way');?> (<?=airport_transfers_lang_e('From airport');?>)</option>
          <option value="to_airport"><?=airport_transfers_lang_e('One way');?> (<?=airport_transfers_lang_e('To airport');?>)</option>
        </select>
      </div>
  </div>
  <div class="clear"></div><br>
  <div class="form-group">
      <label class="col-md-4 col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('No. of adults');?><span class="text-danger">*</span></label>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <input type="text" id="adults" class="form-control" value="" placeholder="">
      </div> 
      
      <label class="col-md-3 col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('No. of children');?><span class="text-danger">*</span></label>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <input type="text" id="children" class="form-control" value="" placeholder="">
      </div>
  </div>
  <div class="clear"></div><br>
  <div class="form-group">  
      <label class="col-md-4 col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('No. of koffers');?><span class="text-danger">*</span></label>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <input type="text" id="koffers" class="form-control" value="" placeholder="">
      </div> 
      
      <?php if(strtolower($params['resort'])==='winter') : ?>
      <label class="col-md-3 col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('No. of ski/snowboards');?><span class="text-danger">*</span></label>
      <div class="col-md-2 col-sm-2 col-xs-4">
        <input type="text" id="ski" class="form-control" value="" placeholder="">
      </div>          
      <?php endif; ?>
  </div>
  <div class="clear"></div><hr>
  
  <div id="arrival">
    <h4><span class="text-success"><?=airport_transfers_lang_e('After arrival');?></span> <span class="small">(<?=airport_transfers_lang_e('From airport');?>)</span></h4> 

    <div class="form-group">
        <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Date of arrival');?><span class="text-danger">*</span></label>
        <div class="col-sm-3 col-xs-4">
          <input type="text" id="arrival_date" value="" class="date form-control" placeholder="">
        </div>
    </div>
    <div class="clear"></div><br>
    <div class="form-group">
        <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Time of arrival');?><span class="text-danger">*</span></label>
        <div class="col-sm-3 col-xs-4">
          <select id="arrival_hour" class="form-control">
            <option value="-"><?=airport_transfers_lang_e('Hour')?></option>
            <?php foreach ($hours as $hour) : ?>
            <option value="<?=$hour?>"><?=$hour?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col-sm-3 col-xs-4">
          <select id="arrival_minutes" class="form-control">
            <option value="-"><?=airport_transfers_lang_e('Min')?></option>
            <?php foreach ($minutes as $minute) : ?>
            <option value="<?=$minute?>"><?=$minute?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col-sm-2 col-xs-4">
          <?php if($am_pm!==FALSE) : ?>
          <select id="arrival_us_time" class="form-control">
            <?=$am_pm?>
          </select>
          <?php endif; ?>
        </div>
    </div>
    <div class="clear"></div><br>
    <div class="form-group">
        <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Airport')?><span class="text-danger">*</span></label>
        <div class="col-sm-6 col-xs-12">
          <select id="arrival_airport" class="form-control">          
            <option value="-"><?=airport_transfers_lang_e('select')?></option>
            <?php foreach ($options as $option) : ?>
            <option value="<?=$option?>"><?=$option?></option>
            <?php endforeach; ?>
          </select>
        </div>
    </div>
    <div class="clear"></div><br>
    <div class="form-group">
        <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Flight number')?><span class="text-danger">*</span></label>
        <div class="col-sm-6 col-xs-12">
          <input type="text" class="form-control" id="arrival_flight" value="" placeholder="">
        </div>
    </div>
    <div class="clear"></div><br>
  </div>
  
  <div id="departure">
    <h4><span class="text-success"><?=airport_transfers_lang_e('Leaving')?></span> <span class="small">(<?=airport_transfers_lang_e('To airport');?>)</span></h4> 

    <div class="form-group">
        <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Date of departure')?><span class="text-danger">*</span></label>
        <div class="col-sm-3 col-xs-4">
          <input type="text" id="out_date" value="" class="date form-control" placeholder="">
        </div>
    </div>
    <div class="clear"></div><br>
    <div class="form-group">
        <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Time of departure');?><span class="text-danger">*</span></label>
        <div class="col-sm-3 col-xs-4">
          <select id="out_hour" class="form-control">
            <option value="-"><?=airport_transfers_lang_e('Hour')?></option>
            <?php foreach ($hours as $hour) : ?>
            <option value="<?=$hour?>"><?=$hour?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col-sm-3 col-xs-4">
          <select id="out_minutes" class="form-control">
            <option value="-"><?=airport_transfers_lang_e('Min')?></option>
            <?php foreach ($minutes as $minute) : ?>
            <option value="<?=$minute?>"><?=$minute?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col-sm-2 col-xs-4">
          <?php if($am_pm!==FALSE) : ?>
          <select id="out_us_time" class="form-control">
            <?=$am_pm?>
          </select>
          <?php endif; ?>
        </div>
    </div>
    <div class="clear"></div><br>
    <div class="form-group">
        <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Airport')?><span class="text-danger">*</span></label>
        <div class="col-sm-6 col-xs-12">
          <select id="out_airport" class="form-control">          
            <option value="-"><?=airport_transfers_lang_e('select')?></option>
            <?php foreach ($options as $option) : ?>
            <option value="<?=$option?>"><?=$option?></option>
            <?php endforeach; ?>
          </select>
        </div>
    </div>
    <div class="clear"></div><br>
    <div class="form-group">
        <label class="col-sm-4 col-xs-12 control-label"><?=airport_transfers_lang_e('Flight number')?><span class="text-danger">*</span></label>
        <div class="col-sm-6 col-xs-12">
          <input type="text" id="out_flight" class="form-control" value="" placeholder="">
        </div>
    </div>
    <div class="clear"></div><br>
  </div>
  
  <div class="clear"></div><hr>

  <h4 class="text-success"><?=airport_transfers_lang_e('Address in')?> <?=airport_transfers_lang_e(get_option('ns-airport-transfers-country'))?></h4> 
  
  <div class="clear"></div>
  
  <div class="form-group">
    <label class="col-md-4 col-sm-12 col-xs-12"><?=airport_transfers_lang_e('Address of hotel/accommodation')?>: <span class="text-danger">*</span></label>
    <div class="col-md-8 col-sm-12 col-xs-12">
      <textarea id="adress" class="form-control" rows="2" placeholder=""></textarea>
    </div>      
  </div>
  
  <div class="clear"></div><br>
  
  <div class="form-group">
    <label class="col-md-4 col-sm-12 col-xs-12"><?=airport_transfers_lang_e('Remarks/instructions')?>:</label>
    <div class="col-md-8 col-sm-12 col-xs-12">
      <textarea id="instructions" class="form-control" rows="2" placeholder=""></textarea>
    </div>
  </div>
  <br><br>
  <div id="submit" class="btn btn-default"><?=airport_transfers_lang_e('Send')?></div>
  
</form>

<?php if($params['show-contacts']==='true') : ?>
<div class="clear"></div><br>
<i><?php printf(airport_transfers_lang('If you need transfer from airport to another destination, wich is not specified please call us at %s or send an e-mail to %s'), get_option('ns-airport-transfers-contact-phone'), get_option('ns-airport-transfers-contact-email'))?></i>
<?php endif; ?>