<?php defined( 'ABSPATH' ) or die( 'I can\'t do anything alone! Sorry!' );

if( !class_exists('NS_Airport_Transfers') ) {
  
  class NS_Airport_Transfers {
  
    private static $initiated = false;
    
    private static $class       = 'NS_Airport_Transfers';
    public static  $textdomain  = 'ns-airport-transfers';
    public static  $prefix      = 'ns_';
    
    public static function init() {
      if ( !self::$initiated ) {
              self::init_hooks();
      }
    }
    
    /**
    * Initializes WordPress hooks
    */
    private static function init_hooks() { 
      // add javascript for the form
      add_action('wp_footer', array(self::$class, self::$prefix.'form_js')); 
      // form shortcode      
      add_shortcode('airport-transfers-form', array(self::$class, self::$prefix.'form'));
      self::$initiated = true;
    } 
    
    
    public static function ns_form_js() { ?>
<script type="text/javascript">
 
jQuery(document).ready(function($) {
  
    var prefix = '#ns-airport-transfer-form ';
    
    // load datepicker
    if((prefix+".date").length > 0 ){
      $(prefix+".date" ).datepicker();
    }
    
    $(prefix+'#transfer_type').on('change', function(){
      let val= $(prefix+'#transfer_type option:selected').val();
      if(val==='from_airport') {
        $(prefix+"#arrival").css('display','block');
        $(prefix+"#departure").css('display','none');
      } else if(val==='to_airport') {
        $(prefix+"#arrival").css('display','none');
        $(prefix+"#departure").css('display','block');
      } else {
        $(prefix+"#arrival").css('display','block');
        $(prefix+"#departure").css('display','block');
      }
    });
    

    $(prefix+'#submit').on('click', function(){ 
      
      if(!ns_form_validate(prefix+'#name','<?=airport_transfers_lang_e('Your Name')?>')) {
        return;
      }
      
      if(!ns_form_validate(prefix+'#email','<?=airport_transfers_lang_e('Email Address')?>')) {
        return;
      }
      
      if(!ns_form_validate(prefix+'#phone','<?=airport_transfers_lang_e('Mobile')?>')) {
        return;
      }
      
      $('body').last('div').after('<div id="loading" class="loading"></div>');
      
      $.post( '<?=admin_url('admin-ajax.php?action=send')?>', { 
        
        // form data
        lang: '<?php echo function_exists('pll_current_language')?pll_current_language('locale'):get_locale(); ?>',        
        resort: $(prefix+"#resort").val(),        
        time_format: $(prefix+"#time_format").val(),        
        
        // personal data
        name: $(prefix+"#name").val(), 
        email: $(prefix+"#email").val(),
        phone: $(prefix+"#phone").val(),
        like_to: $(prefix+'input[name=like_to]:checked').val(),
        transfer_type: $(prefix+"#transfer_type option:selected").val(),
        adults: $(prefix+"#adults").val(),
        children: $(prefix+"#children").val(),
        koffers: $(prefix+"#koffers").val(),
        ski: ($(prefix+"#ski").length>0) ? $("#ski").val() : '',
        
        // arrival data
        arrival_date: $(prefix+"#arrival_date").val(),
        arrival_hour: $(prefix+"#arrival_hour option:selected").val(),
        arrival_minutes: $(prefix+"#arrival_minutes option:selected").val(),
        arrival_us_time: ($(prefix+"#arrival_us_time").length>0) ? $("#arrival_us_time option:selected").val() : '',
        arrival_airport: $(prefix+"#arrival_airport option:selected").val(),
        arrival_flight: $(prefix+"#arrival_flight").val(),
        
        // departure data
        out_date: $(prefix+"#out_date").val(),
        out_hour: $(prefix+"#out_hour option:selected").val(),
        out_minutes: $(prefix+"#out_minutes option:selected").val(),
        out_us_time: ($(prefix+"#out_us_time").length>0) ? $("#out_us_time option:selected").val() : '',
        out_airport: $(prefix+"#out_airport option:selected").val(),
        out_flight: $(prefix+"#out_flight").val(),
        
        // address and instructions
        adress: $(prefix+"#adress").val(),
        instructions: $(prefix+"#instructions").val()
      }).done(function(data) {
        let response = $.parseJSON(data);
        if(response.status==='error') {
          $('#modal-error-message').text(response.data); 
          $('#modal-error').modal();
          return;
        }
        
        $('html, body').animate({
          scrollTop: $(".main-container").offset().top
        }, 500);
                
        setTimeout(function(){ 
            $("#ns-airport-transfer-form").empty();
            $("#ns-airport-transfer-form").html('<h3 class="text-success">'+response.data+'</h3>');
        },500);
      }).fail(function() {
        $('#modal-error-message').text('<?=airport_transfers_lang_e('System Error!')?>'); 
        $('#modal-error').modal();
      })
      .always(function() {
        $("#loading").remove();
      });
    });
    
    function ns_form_validate(el,message) {
      if($(el).val()==='') {
        $('html, body').animate({
          scrollTop: $(".main-container").offset().top
        }, 500);
        $('#modal-error-message').text('<?=airport_transfers_lang_e('Please input')?> '+message); 
        $('#modal-error').modal();
        return false;
      }
      return true;
    }

  });

</script>
    <?php }
        
    /**
    * Main function to display and process the form
    * 
    * option provided are: 
    * resort - if summer [no. of ski] field is skipped, 
    * country - concatenated with [Addres in] label, default is [add country option please]
    * time-format - if 12 we use the 12 Hour format, default is 24
    * 
    * content provides comma separeted airports for the dropdown, default is one option [add Airports in content please]
    */
    public static function ns_form($atts, $content = '') {
      
      $params = shortcode_atts( array(
        'resort' => get_option('ns-airport-transfers-resort'),
        'time-format' => get_option('ns-airport-transfers-time-format'),
        'show-contacts' => 'true'
      ), $atts);
      
      // adjust dropdowns depending on Time format (12 or 24 Hours)
      if(strtolower($params['time-format'])==='12') :
        for($i=1; $i<=12; $i++) :
          $hours[] = strval($i);
        endfor;
        $am_pm = '<option>AM/PM</option>
                <option value="AM">AM</option>
                <option value="PM">PM</option>';
      else :
        for($i=0; $i<=23; $i++) :
          $hours[] = ($i<10) ? '0' . strval($i) : strval($i);
        endfor;
        $am_pm = FALSE;
      endif;
      
      // adjust minutes dropdown
      for($i=0; $i<=55; $i++) :
        if($i%5===0) :
          $minutes[] = ($i<10) ? '0' . strval($i) : strval($i);
        endif;
      endfor;
      
      // process the content from the shortcode
      $options = explode(",", $content);      
      $options = (sizeof($options)>0) ? $options : array();
      
      ob_start();
      require_once( plugin_dir_path(__FILE__) . 'views/form.php' );
      require_once( plugin_dir_path(__FILE__) . 'views/modal-error.php' );
      return ob_get_clean();
    }
    
  } // class ends here 
}

