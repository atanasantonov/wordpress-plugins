<?php defined( 'ABSPATH' ) or die( 'I can\'t do anything alone! Sorry!' );

// initialize texts
$strings = array(
  get_option('ns-airport-transfers-country'),
  'If you need transfer from airport to another destination, wich is not specified please call us at %s or send an e-mail to %s',
  'Form elements',
  'Information about you',
  'Your name',
  'Email address',
  'On this Email you will recieve confirmation',
  'Mobile',
  'It\'s mandatory to input your phone number',
  'I would like to',
  'Make reservation',
  'Request more information',
  'Transfer type',
  'Two way',
  'One way',
  'From airport',
  'To airport',
  'No. of adults',
  'No. of children',
  'No. of koffers',
  'No. of ski/snowboards',
  'After arrival',
  'Date of arrival',
  'Time of arrival',
  'Hour',
  'Min',
  'Airport',
  'select',
  'Flight number',
  'Leaving',
  'Date of departure',
  'Time of departure',
  'Address in',
  'Address of hotel/accommodation',
  'Remarks/instructions',
  'Send',
  'Request',
  'Thank you for your submission! We will answer as soon as possible!',
  'Error',
  'System Error!',
  'Incorrect data!',
  'Invalid email address!',
  'Please input',
  'close',
  'reservation',
  'more_information',
  'two_way',
  'from_airport',
  'to_airport',
);

// we push the strings in array('text_to_display'=>'Text to display')
// to use the strings in any time later
foreach ($strings as $string) {
  $index = str_replace(' ', '_', mb_strtolower($string));
  $texts[$index] = $string; 
}
