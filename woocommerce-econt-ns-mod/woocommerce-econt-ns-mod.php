<?php
/*
Plugin Name: Econt Express - WooCommerce (NS Mod)
Plugin URI: https://nantstudio.bg
Description: Modified version of mreja.net's original plugin
Version: 1.0
Author: Martin Vasilev (modifications by Atanas Antonov)
Author URI: http://mreja.net
Text Domain: woocommerce-econt-ns-mod
Domain Path: /languages/
*/

defined( 'ABSPATH' ) or die( 'I can\'t do anything alone! Sorry!' );

if (!defined('ECONT_PLUGIN_DIR'))
    define('ECONT_PLUGIN_DIR', dirname(__FILE__));

if (!defined('ECONT_PLUGIN_ROOT_PHP'))
    define('ECONT_PLUGIN_ROOT_PHP', dirname(__FILE__) . '/' . basename(__FILE__));

if (!defined('ECONT_PLUGIN_ADMIN_DIR'))
    define('ECONT_PLUGIN_ADMIN_DIR', dirname(__FILE__) . '/admin');

if (!defined('ECONT_PLUGIN_URL'))
    define('ECONT_PLUGIN_URL', plugin_dir_url(__FILE__));

if (!defined('ECONT_VERSION_KEY'))
    define('ECONT_VERSION_KEY', 'woocommerce-econt_version');

if (!defined('ECONT_VERSION_NUM'))
    define('ECONT_VERSION_NUM', '1.0');

/* http://tvbox.local/wp-admin/admin-ajax.php?
 * sender_door_or_office=OFFICE
 * &sender_door=9700;Шумен;
 * ;Array;
 * Array;;
 * &payment_side=SENDER
 * &weight=0.8
 * &pack_count=1
 * &order_cd=1
 * &order_cd_amount=135
 * &order_pay_after=accept
 * &dc=0&order_oc=0
 * &order_cd_amount=135
 * &instruction_returns=shipping_returns
 * &sms=0
 * &invoice=0
 * &dc_cp=0&instructions_take=0
 * &instructions_give=0
 * &instructions_return=0
 * &instructions_services=0
 * &city_courier=0
 * &econt_city_courier=0
 * &delivery_day_id=0
 * &partial_delivery=0
 * &inventory=0
 * &products[0][product_id]=288&products[0][name]= */

if (!class_exists('Econt_Express')) {
    
    class Econt_Express {
        
        const VERSION = ECONT_VERSION_NUM;
        const ECONT_VERSION = ECONT_VERSION_NUM;
        const CAPABILITY = "edit_econt_express";
        const DEMO = false;
        
        public static function init() {
            
            require_once(ECONT_PLUGIN_DIR . '/inc/class-mysql-econt.php');  //create econt tables and do all mysql queries
            require_once(ECONT_PLUGIN_ADMIN_DIR . '/class-sm-econt.php');   //adds shipping method econt to woocommerce settings 
            require_once(ECONT_PLUGIN_ADMIN_DIR . '/class-order-econt.php'); //order details create loading (ot tuk zarejdam i js i css scriptovete)
            
            require_once(ECONT_PLUGIN_DIR . '/inc/class-ajax-econt.php');
            
            // require_once(ECONT_PLUGIN_DIR . '/inc/class-checkout-econt.php'); //class s funkcii dobavqshti meta poleta za econt v checkout
            // require_once(ECONT_PLUGIN_DIR . '/lib/plugin-update-checker/plugin-update-checker.php'); //updates checker
            
            add_action('plugins_loaded', array(
                'Econt_Express',
                'plugins_loaded'
            ));
            
            register_activation_hook(__FILE__, array(
                'Econt_mySQL',
                'createTables'
            ));
            
            //register_deactivation_hook(__FILE__, array('Econt_mySQL', 'plugin_deactivation'));            
            //$econtUpdateChecker = Puc_v4_Factory::buildUpdateChecker('http://wpus.mreja.net/updater/?action=get_metadata&slug=woocommerce-econt',__FILE__,'woocommerce-econt');
        }
        
        public static function plugins_loaded() {
            load_plugin_textdomain('woocommerce-econt', false, dirname(plugin_basename(__FILE__)) . '/languages/');
        }
    }
}

if( is_admin() ) {
    Econt_express::init();
}

