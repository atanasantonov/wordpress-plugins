<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Econt_Ajax {

    public function __construct(){
        // Test #1
        add_action( 'wp_ajax_nopriv_handle_ajax', array( &$this, 'handle_ajax' ) );

        // Test #2 
        add_action( 'wp_ajax_handle_ajax', array(&$this, 'handle_ajax') ); //with and without '&' before $this

        //add the build in wordpress ajax url so we can use it in out js files var ajaxurl
        add_action('wp_head',array(&$this, 'pluginname_ajaxurl') );
    }


    public function handle_ajax(){

        global $wpdb;
        
        $result = array();
        $res = array();

        $econt_mysql = new Econt_mySQL;

        if(isset($_GET['city'])){
            $city = $_GET['city'];
            $results = $econt_mysql->getCityByName($city);
            foreach ($results as $row) {

                $res['label'] 		= $row['type'].' '.$row['name'].'[ п.к.:'.$row['post_code'].']';
                $res['value'] 		= $row['name'];
                $res['id'] 			= $row['city_id'];
                $res['post_code'] 	= $row['post_code'];

                $result[] = $res;
            }
        }

        if(isset($_GET['office_city_id'])){

            $city_id = $_GET['office_city_id'];

            $results = $econt_mysql->getOfficesByCityId($city_id,'',$_GET['delivery_type']);

            foreach ($results as $row) {

                $res['value'] = $row['name'].' ['.$row['address'].']';
                $res['id'] = $row['office_code'];

                $result[] = $res;

            }
        }


        if(isset($_GET['machine_city_id'])){

            $city_id = $_GET['machine_city_id'];
            $is_machine = 1;
            $results = $econt_mysql->getOfficesByCityId($city_id, $is_machine);

            foreach ($results as $row) {

                $res['value'] = $row['name'].' ['.$row['address'].']';
                $res['id'] = $row['office_code'];

                $result[] = $res;

            }

        }


        if(isset($_GET['door_city_id']) && $_GET['type'] == 'street'){

            $city_id = $_GET['door_city_id'];
            $street_name = $_GET['door_street_name'];

            $results = $econt_mysql->getStreetsByCityId($city_id, $street_name);

            foreach ($results as $row) {

                $res['value'] = $row['name'];
                $res['id'] = $row['street_id'];

                $result[] = $res;

            }

        }

        if(isset($_GET['door_city_id']) && $_GET['type'] == 'quarter'){

            $city_id = $_GET['door_city_id'];
            $quarter_name = $_GET['door_quarter_name'];

            $results = $econt_mysql->getQuartersByCityId($city_id, $quarter_name);

            foreach ($results as $row) {

                $res['value'] = $row['name'];
                $res['id'] = $row['quarter_id'];

                $result[] = $res;

            }

        }

        //refresh Econt addresses database
        if(isset($_GET['refresh_data']) && isset($_GET['username']) && isset($_GET['password'])){

            $username 	= $_GET['username'];
            $password 	= $_GET['password'];
            $live		= $_GET['live'];
            $results = $econt_mysql->refreshData($username, $password, $live);
            
            if($results['error']){
                $result[] =  $results['error'];
            } else {
                $result[] = __('refresh data success', 'woocommerce-econt');	
            }

        }

        //refresh Econt addresses database
        if(isset($_GET['sync_profile']) && isset($_GET['username']) && isset($_GET['password'])){

            $username 		= $_GET['username'];
            $password 		= $_GET['password'];
            $live			= $_GET['live'];
            $profile 		= $econt_mysql->getProfile($username, $password, $live);
            $access_clients = $econt_mysql->getClients($username, $password, $live);

            if( array_key_exists('error', $profile) || array_key_exists('error', $access_clients) ){
                $result[] = 'profile Error: ' . $profile['error'] . 'access_clients Error: ' . $access_clients['error'] ;
            }else{
                $profile = Econt_mySQL::xml2array($profile);
                $access_clients = Econt_mySQL::xml2array($access_clients);
                //Econt_mySQL::write_log($profile);
                update_option('econt_profile', $profile);
                update_option('econt_access_clients', $access_clients);
                $result[] = __('refresh data success', 'woocommerce-econt');	
            }
        }

        //order loading
        if(isset($_GET['action2'])){

            global $woocommerce;
            
            $woocommerce->shipping;
            $wc_econt = new WC_Econt_Shipping_Method;
            $econt_order = new Econt_Admin_Order;
            
                    if( $_GET['action2'] == 'only_calculate_loading' || $_GET['action2'] == 'create_loading' ) {

                        //if(!$_GET['order_id']){ };

                        (isset($_GET['order_id']) ?: $_GET['order_id'] = -1); //if there is no order id yet set order id to -1
                        $_GET['weight'] = $this->getIfSet($_GET['weight'], $woocommerce->cart->cart_contents_weight);

                        if($_GET['weight'] == 0) {

                            $result = array(
                                                'order_id'    				=> $_GET['order_id'],
                                                'econt_shipping_expenses' 	=>  array('total_shipping_cost' => '',
                                                                                      'customer_shipping_cost' => __('calculating', 'woocommerce-econt'),
                                                                                  'currency_symbol'	=> '',
                                                                ),
                                        );	

                        } else {

            $data = array();

            $data['client']['username'] 						= $_GET['username'] 				= $this->getIfSet($_GET['username'], $wc_econt->username);
            $data['client']['password'] 						= $_GET['password'] 				= $this->getIfSet($_GET['password'], $wc_econt->password);
            $data['live'] 										= $_GET['live'] 					= $this->getIfSet($_GET['live'], $wc_econt->live);

            $data['system']['request_type'] 					= 'shipping';
            $data['system']['response_type'] 					= 'XML';

            $data['loadings']['row']['sender']['name'] 			= $_GET['sender_name'] 				= $this->getIfSet($_GET['sender_name'], $wc_econt->company);
            $data['loadings']['row']['sender']['name_person'] 	= $_GET['sender_name_person'] 		= $this->getIfSet($_GET['sender_name_person'], $wc_econt->name);
            $data['loadings']['row']['sender']['phone_num'] 	= $_GET['sender_phone_num'] 		= $this->getIfSet($_GET['sender_phone_num'], $wc_econt->phone);

            $_GET['sender_door_or_office'] 	= $this->getIfSet($_GET['sender_door_or_office'], $wc_econt->send_from);

            if($_GET['sender_door_or_office'] == 'DOOR' || $_GET['sender_door_or_office'] == 'DOOR2' ){

                if($_GET['sender_door_or_office'] == 'DOOR2'){
                    $address 							= explode(';', $_GET['sender_door']);
                }else{
                    $address 							= explode(';', $wc_econt->address);
                }

                $data['loadings']['row']['sender']['city'] 			= $_GET['sender_city'] 				= $this->getIfSet($_GET['sender_city'], $address[1]);
                $data['loadings']['row']['sender']['post_code'] 	= $_GET['sender_post_code'] 		= $this->getIfSet($_GET['sender_post_code'], $address[0]);
                $data['loadings']['row']['sender']['street'] 		= $_GET['sender_street'] 			= $this->getIfSet($_GET['sender_street'], $address[3]);
                $data['loadings']['row']['sender']['quarter'] 		= $_GET['sender_quarter'] 			= $this->getIfSet($_GET['sender_quarter'], $address[2]);
                $data['loadings']['row']['sender']['street_num'] 	= $_GET['sender_street_num'] 		= $this->getIfSet($_GET['sender_street_num'], $address[4]);
                $data['loadings']['row']['sender']['street_bl'] 	= $_GET['sender_street_bl'] 		= $this->getIfSet($_GET['sender_street_bl']);
                $data['loadings']['row']['sender']['street_vh'] 	= $_GET['sender_street_vh'] 		= $this->getIfSet($_GET['sender_street_vh']);
                $data['loadings']['row']['sender']['street_et'] 	= $_GET['sender_street_et'] 		= $this->getIfSet($_GET['sender_street_et']);
                $data['loadings']['row']['sender']['street_ap'] 	= $_GET['sender_street_ap'] 		= $this->getIfSet($_GET['sender_street_ap']);
                $data['loadings']['row']['sender']['street_other'] 	= $_GET['sender_street_other'] 		= $this->getIfSet($_GET['sender_street_other'], $address[5]);

            } elseif($_GET['sender_door_or_office'] == 'OFFICE') {

                $data['loadings']['row']['sender']['city'] 			= $_GET['sender_city'] 				= $this->getIfSet($_GET['sender_city'], $wc_econt->office_town);
                $data['loadings']['row']['sender']['post_code'] 	= $_GET['sender_post_code'] 		= $this->getIfSet($_GET['sender_post_code'], $wc_econt->office_postcode);
                $data['loadings']['row']['sender']['office_code'] 	= $_GET['sender_office_code'] 		= $this->getIfSet($_GET['sender_office_code'], $wc_econt->office_code);

            } elseif($_GET['sender_door_or_office'] == 'MACHINE') {

                $data['loadings']['row']['sender']['city'] 			= $_GET['sender_city'] 				= $this->getIfSet($_GET['sender_city'], $wc_econt->machine_town);
                $data['loadings']['row']['sender']['post_code'] 	= $_GET['sender_post_code'] 		= $this->getIfSet($_GET['sender_post_code'], $wc_econt->machine_postcode);
                $data['loadings']['row']['sender']['office_code'] 	= $_GET['sender_office_code'] 		= $this->getIfSet($_GET['sender_office_code'], $wc_econt->machine_code);

            }


            $data['loadings']['row']['receiver']['name'] 				= $_GET['receiver_name'];
            $data['loadings']['row']['receiver']['name_person'] 		= $_GET['receiver_name_person'];
            $data['loadings']['row']['receiver']['phone_num'] 			= $_GET['receiver_phone_num'];
            $data['loadings']['row']['receiver']['city'] 				= $_GET['receiver_city'];
            $data['loadings']['row']['receiver']['post_code'] 			= $_GET['receiver_post_code'];

            if($_GET['receiver_shipping_to'] == 'DOOR'){


            $data['loadings']['row']['receiver']['office_code'] 		= '';
            $data['loadings']['row']['receiver']['street'] 				= $_GET['receiver_street'];
            $data['loadings']['row']['receiver']['quarter'] 			= $_GET['receiver_quarter'];
            $data['loadings']['row']['receiver']['street_num'] 			= $_GET['receiver_street_num'];
            $data['loadings']['row']['receiver']['street_bl'] 			= $_GET['receiver_street_bl'];
            $data['loadings']['row']['receiver']['street_vh'] 			= $_GET['receiver_street_vh'];
            $data['loadings']['row']['receiver']['street_et'] 			= $_GET['receiver_street_et'];
            $data['loadings']['row']['receiver']['street_ap'] 			= $_GET['receiver_street_ap'];
            $data['loadings']['row']['receiver']['street_other'] 		= $_GET['receiver_street_other'];

            }elseif($_GET['receiver_shipping_to'] == 'OFFICE' || $_GET['receiver_shipping_to'] == 'MACHINE'){

            $data['loadings']['row']['receiver']['office_code'] 		= $_GET['receiver_office_code'];
            $data['loadings']['row']['receiver']['street'] 				= '';
            $data['loadings']['row']['receiver']['quarter'] 			= '';
            $data['loadings']['row']['receiver']['street_num'] 			= '';
            $data['loadings']['row']['receiver']['street_bl'] 			= '';
            $data['loadings']['row']['receiver']['street_vh'] 			= '';
            $data['loadings']['row']['receiver']['street_et'] 			= '';
            $data['loadings']['row']['receiver']['street_ap'] 			= '';
            $data['loadings']['row']['receiver']['street_other'] 		= '';

            }

            $_GET['payment_side'] 			= $this->getIfSet($_GET['payment_side'], $wc_econt->payment_side);
            $_GET['sender_payment_method'] 	= $this->getIfSet($_GET['sender_payment_method'], $wc_econt->client_payment_type);
            //$_GET[''] 					= $this->getIfSet($_GET[''], $wc_econt->client_voucher);
            //$_GET[''] 					= $this->getIfSet($_GET[''], $wc_econt->client_bonus_points);
            $_GET['order_cd'] 				= $this->getIfSet($_GET['order_cd'], $wc_econt->cd);
            $_GET['cd_agreement_num'] 		= $this->getIfSet($_GET['cd_agreement_num'], $wc_econt->client_cd_num);
            $_GET['free_shipping_sum'] 		= $this->getIfSet($_GET['free_shipping_sum'], $wc_econt->free_shipping_sum);
            $_GET['free_shipping_weight'] 	= $this->getIfSet($_GET['free_shipping_weight'], $wc_econt->free_shipping_weight);
            $_GET['free_shipping_count'] 	= $this->getIfSet($_GET['free_shipping_count'], $wc_econt->free_shipping_count);
            $_GET['order_oc'] 				= $this->getIfSet($_GET['order_oc'], $wc_econt->oc);
            $_GET['partial_delivery'] 		= $this->getIfSet($_GET['partial_delivery'], $wc_econt->partial_delivery);
            $_GET['city_courier'] 			= $this->getIfSet($_GET['city_courier'], $wc_econt->city_courier);
            $_GET['dc'] 					= $this->getIfSet($_GET['dc'], $wc_econt->dc);
            $_GET['dc_cp'] 					= $this->getIfSet($_GET['dc_cp'], $wc_econt->dc_cp);
            $_GET['sms'] 					= $this->getIfSet($_GET['sms'], $wc_econt->sms);
            $_GET['invoice'] 				= $this->getIfSet($_GET['invoice'], $wc_econt->invoice);
            $_GET['order_pay_after'] 		= $this->getIfSet($_GET['order_pay_after'], $wc_econt->pay_after);
            $_GET['instruction_returns'] 	= $this->getIfSet($_GET['instruction_returns'], $wc_econt->instruction_returns);
            $_GET['inventory'] 				= $this->getIfSet($_GET['inventory'], $wc_econt->inventory);
            $_GET['instructions_take'] 		= $this->getIfSet($_GET['instructions_take'], $wc_econt->instructions_take);
            $_GET['instructions_give'] 		= $this->getIfSet($_GET['instructions_give'], $wc_econt->instructions_give);
            $_GET['instructions_return'] 	= $this->getIfSet($_GET['instructions_return'], $wc_econt->instructions_return);
            $_GET['instructions_services'] 	= $this->getIfSet($_GET['instructions_services'], $wc_econt->instructions_services);
            //$_GET['shipping_payment1'] 		= $this->getIfSet($_GET['shipping_payment1'], $wc_econt->shipping_payment1);
            //$_GET['shipping_payment2'] 		= $this->getIfSet($_GET['shipping_payment2'], $wc_econt->shipping_payment2);
            $_GET['shipping_payments'] 		= $this->getIfSet($_GET['shipping_payments'], $wc_econt->shipping_payments);

            $_GET['receiver_name'] 			= $this->getIfSet($_GET['receiver_name']);
            $_GET['receiver_name_person'] 	= $this->getIfSet($_GET['receiver_name_person']);
            $_GET['description'] 			= $this->getIfSet($_GET['description']);
            $_GET['delivery_days'] 			= $this->getIfSet($_GET['delivery_days'], $wc_econt->delivery_days);
            $_GET['delivery_day_id']		= $this->getIfSet($_GET['delivery_day_id']);
            $_GET['priority_time'] 			= $this->getIfSet($_GET['priority_time']);

            $_GET['currency'] 				= get_woocommerce_currency();
            $_GET['currency_symbol'] 		= get_woocommerce_currency_symbol();

            //ako se izchislqva poruchka
            if((int)$_GET['order_id'] > 0) {
                $wpc = $econt_order->econt_order_products($_GET['order_id']);
                $_GET['order_cd_amount'] 		= $this->getIfSet($_GET['order_cd_amount'], $wpc['price']);
                $_GET['weight'] 				= $this->getIfSet($_GET['weight'], $wpc['weight']);
                $_GET['count'] 					= $this->getIfSet($_GET['count'], $wpc['count']);
            }
            //ako se izchislqva koshnica
            if($_GET['order_id'] == -1) {
                if($_GET['payment_method_cod'] == 0){
                $_GET['order_cd'] = 0;
                }
                //$_GET['order_cd_amount'] 		= $woocommerce->cart->total;
                $_GET['order_cd_amount'] 		= round(floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_subtotal() ) ), 2);
                $_GET['weight'] 				= $woocommerce->cart->cart_contents_weight;
                $_GET['count'] 					= $woocommerce->cart->cart_contents_count;

            //echo 'order_cd: '.$_GET['order_cd'] .' payment_cod: ' .$_GET['payment_method_cod'];
            }
            $_GET['order_oc_amount'] 		= $this->getIfSet($_GET['order_oc_amount'], $_GET['order_cd_amount']);
            
            $cd_type 	= 'GET';


            if ((int)$_GET['sms'] == 1) {
            $sms_no = $_GET['receiver_phone_num'];
            } else {
            $sms_no = '';
            }

            $data['loadings']['row']['receiver']['sms_no'] = $sms_no;


            if((float)$_GET['weight'] <= 100){
            $data['loadings']['row']['shipment']['shipment_type'] 		= 'PACK';
            }else{
            $data['loadings']['row']['shipment']['shipment_type'] 		= 'CARGO';
            $data['loadings']['row']['shipment']['cargo_code']			= 81;	
            }

            $tariff_sub_code = preg_replace('/\d/','',$_GET['sender_door_or_office']).'_'.$_GET['receiver_shipping_to'];
            $tariff_sub_code = str_replace('MACHINE', 'OFFICE', $tariff_sub_code);

            $tariff_code = 0;

            if (!empty($_GET['econt_city_courier']) && $_GET['receiver_shipping_to'] == 'DOOR') {
            $tariff_code = 1;
            } elseif ($tariff_sub_code == 'OFFICE_OFFICE' || $tariff_sub_code == 'MACHINE_MACHINE' || $tariff_sub_code == 'MACHINE_OFFICE' || $tariff_sub_code == 'OFFICE_MACHINE') {
            $tariff_code = 2;
            } elseif ($tariff_sub_code == 'OFFICE_DOOR' || $tariff_sub_code == 'DOOR_OFFICE' || $tariff_sub_code == 'MACHINE_DOOR' || $tariff_sub_code == 'DOOR_MACHINE' ) {
            $tariff_code = 3;
            } elseif ($tariff_sub_code == 'DOOR_DOOR') {
            $tariff_code = 4;
            }

            $data['loadings']['row']['shipment']['description'] 		= $_GET['description'];
            $data['loadings']['row']['shipment']['pack_count'] 			= $_GET['pack_count'];
            $data['loadings']['row']['shipment']['weight'] 				= $_GET['weight'];

            if($_GET['sender_door_or_office'] == 'MACHINE' || $_GET['receiver_shipping_to'] == 'MACHINE'){
            if((float)$_GET['weight'] <= 5){
            $data['loadings']['row']['shipment']['aps_box_size'] 		= 'Small';
            }elseif((float)$_GET['weight'] > 5 && $_GET['weight'] <= 10){
            $data['loadings']['row']['shipment']['aps_box_size'] 		= 'Medium';
            }elseif((float)$_GET['weight'] > 10 && $_GET['weight'] <= 50){
            $data['loadings']['row']['shipment']['aps_box_size'] 		= 'Large';
            }
            }

            $data['loadings']['row']['shipment']['tariff_code'] 		= $tariff_code;
            $data['loadings']['row']['shipment']['tariff_sub_code'] 	= $tariff_sub_code;

            if($_GET['order_pay_after'] == 'accept' && $_GET['receiver_shipping_to'] != 'MACHINE'){ 
            $data['loadings']['row']['shipment']['pay_after_accept'] 	= 1;
            $data['loadings']['row']['shipment']['pay_after_test'] 		= 0;
            }elseif($_GET['order_pay_after'] == 'test' && $_GET['receiver_shipping_to'] != 'MACHINE'){
            $data['loadings']['row']['shipment']['pay_after_accept'] 	= 0;
            $data['loadings']['row']['shipment']['pay_after_test'] 		= 1;
            }elseif($_GET['order_pay_after'] == 0 || $_GET['receiver_shipping_to'] == 'MACHINE'){
            $data['loadings']['row']['shipment']['pay_after_accept'] 	= 0;
            $data['loadings']['row']['shipment']['pay_after_test'] 		= 0;
            }

            $data['loadings']['row']['shipment']['instruction_returns'] = $_GET['instruction_returns'];
            $data['loadings']['row']['shipment']['invoice_before_pay_CD'] = $_GET['invoice'];
            //da go dovursha v checkout
            if($_GET['delivery_days'] == 1 && isset($_GET['delivery_day_id'])) {
            $delivery_day = $_GET['delivery_day_id'];
            }else{
            $delivery_day = '';
            }

            $data['loadings']['row']['shipment']['delivery_day'] = $delivery_day;
            //


            //

            if($_GET['priority_time'] == 1 && $_GET['receiver_shipping_to'] == 'DOOR') {
                $priority_time_type = $_GET['priority_time_type'];
                $priority_time_value = $_GET['priority_time_hour'];
            }else{
                $priority_time_type = '';
                $priority_time_value = '';
            }

            $data['loadings']['row']['services']['p'] = array('type' => $priority_time_type, 'value' => $priority_time_value);

            $city_courier_e1 = '';
            $city_courier_e2 = '';
            $city_courier_e3 = '';

            if((int)$_GET['city_courier'] == 1 && $_GET['receiver_shipping_to'] == 'DOOR' && ($_GET['sender_door_or_office'] == 'DOOR' || $_GET['sender_door_or_office'] == 'DOOR2')) {
                if($_GET['econt_city_courier'] == 'e1') {
                    $city_courier_e1 = 'ON';
                }elseif($_GET['econt_city_courier'] == 'e2') {
                    $city_courier_e2 = 'ON';
                }elseif($_GET['econt_city_courier'] == 'e3') {
                    $city_courier_e3 = 'ON';
                }
            }

            $data['loadings']['row']['services']['e1'] = $city_courier_e1;
            $data['loadings']['row']['services']['e2'] = $city_courier_e2;
            $data['loadings']['row']['services']['e3'] = $city_courier_e3;
            //


            //

            if((int)$_GET['dc'] == 1) {
                    $dc = 'ON';
                } else {
                    $dc = '';
                }

                $data['loadings']['row']['services']['dc'] = $dc;

                if ((int)$_GET['dc_cp'] == 1) {
                    $dc_cp = 'ON';
                } else {
                    $dc_cp = '';
                }

                $data['loadings']['row']['services']['dc_cp'] = $dc_cp;

                if ((int)$_GET['count'] > 1 && (int)$_GET['partial_delivery'] == 1) {
                    $data['loadings']['row']['packing_list']['partial_delivery'] = $_GET['partial_delivery'];
                }

                if ($_GET['inventory'] != '0') {
                    $data['loadings']['row']['packing_list']['type'] = $_GET['inventory'];

                    if ($_GET['inventory'] == 'DIGITAL' && $_GET['products']) { //trqbva da suzdan $_GET['products'] v order class
                        foreach ($_GET['products'] as $product) {
                            $data['loadings']['row']['packing_list']['row'][]['e'] = array(
                                'inventory_num' => $product['product_id'],
                                'description'   => $product['name'],
                                'weight'        => $product['weight'],
                                'price'         => $product['price']
                            );
                        }
                    }
                }
            //

            //instructions
            if($_GET['receiver_shipping_to'] != 'MACHINE'){

                if ($_GET['instructions_take']) {
                            $data['loadings']['row']['instructions'][]['e'] = array(
                                'type'     => 'take',
                                'template' => $_GET['instructions_take']
                            );
                }
                if ($_GET['instructions_give']) {
                            $data['loadings']['row']['instructions'][]['e'] = array(
                                'type'     => 'give',
                                'template' => $_GET['instructions_give']
                            );
                }
                if ($_GET['instructions_return']) {
                            $data['loadings']['row']['instructions'][]['e'] = array(
                                'type'     => 'return',
                                'template' => $_GET['instructions_return']
                            );
                }
                if ($_GET['instructions_services']) {
                            $data['loadings']['row']['instructions'][]['e'] = array(
                                'type'     => 'services',
                                'template' => $_GET['instructions_services']
                            );
                }
            }
            //

            if((int)$_GET['order_cd'] == 1){
                $data['loadings']['row']['services']['cd'] 					= array('type' => $cd_type, 'value' => $_GET['order_cd_amount']);
                $data['loadings']['row']['services']['cd_currency'] 		= $_GET['currency'];
                $data['loadings']['row']['services']['cd_agreement_num'] 	= $_GET['cd_agreement_num'];
            }

            //if($_GET['order_oc'] == '1' && $sender_door_or_office != 'MACHINE' && $receiver_shipping_to != 'MACHINE' ){
            if( ($_GET['order_oc'] == 1 && $_GET['sender_door_or_office'] != 'MACHINE' && $_GET['receiver_shipping_to'] != 'MACHINE') || ($_GET['order_oc'] > 1 && $_GET['order_cd_amount'] > $_GET['order_oc'] && $_GET['sender_door_or_office'] != 'MACHINE' && $_GET['receiver_shipping_to'] != 'MACHINE')){
                $data['loadings']['row']['services']['oc'] 					= $_GET['order_oc_amount']; //$oc_amount;
                $data['loadings']['row']['services']['oc_currency'] 		= $_GET['currency'];
            }

            $data['loadings']['row']['payment']['side'] 				= $_GET['payment_side'];


            if($_GET['sender_payment_method'] == 'CASH' || $_GET['sender_payment_method'] == 'BONUS' || $_GET['sender_payment_method'] == 'VOUCHER' ) {
                $data['loadings']['row']['payment']['method'] 				= $_GET['sender_payment_method'];
            }else{
                $data['loadings']['row']['payment']['method'] 				= 'CREDIT';
                $data['loadings']['row']['payment']['key_word'] 			= $_GET['sender_payment_method'];	
            }

            //
            $receiver_share_sum = '';
            $receiver_share_sum_door = '';
            $receiver_share_sum_office = '';
            $free_shipping = '';
                    if ((float)$_GET['free_shipping_sum'] && (float)($_GET['order_cd_amount'] >= (float)$_GET['free_shipping_sum']) || (int)$_GET['free_shipping_count'] && ($_GET['count'] >  $_GET['free_shipping_count']) || (float)$_GET['free_shipping_weight'] && ($_GET['weight'] >= $_GET['free_shipping_weight'])) {

                        $data['loadings']['row']['payment']['side'] = 'SENDER';
                        if($_GET['action2'] == 'only_calculate_loading'){ 
                        $free_shipping = 1;
                        }


                    }elseif (!empty($_GET['shipping_payments'])){
                        //print_r($_GET['shipping_payments']);
                        $shipping_payments = $_GET['shipping_payments'];
                        $order_amount = 0;
                        foreach ($shipping_payments as $shipping_payment) {

                            if ($_GET['order_cd_amount'] >= $shipping_payment['order_amount'] && $shipping_payment['order_amount'] >= $order_amount) {
                                $order_amount = $shipping_payment['order_amount'];
                                $receiver_share_sum_door = $shipping_payment['receiver_amount'];
                                $receiver_share_sum_office = $shipping_payment['receiver_amount_office'];
                            }
                        }



                    }

                    if ($_GET['receiver_shipping_to'] == 'OFFICE' || $_GET['receiver_shipping_to'] == 'MACHINE') {
                        $receiver_share_sum = number_format((float)$receiver_share_sum_office, 2, '.', '');
                    } else {
                        $receiver_share_sum = number_format((float)$receiver_share_sum_door, 2, '.', '');
                    }

                    if ($receiver_share_sum > 0) {
                        $data['loadings']['row']['payment']['side'] = 'SENDER';
                    }
            //			$data['loadings']['row']['payment']['side'] = 'SENDER';

                    $data['loadings']['row']['payment']['receiver_share_sum'] = $receiver_share_sum;
                    $data['loadings']['row']['payment']['share_percent'] = '';

                    if ($data['loadings']['row']['payment']['side'] == 'RECEIVER') {
                        $data['loadings']['row']['payment']['method'] = 'CASH';
                    }

            //





            if( $_GET['action2'] == 'only_calculate_loading' ){

            //$result[] =	$_GET['cd_agreement_num'];
                $data['system']['only_calculate'] = 1;
                $data['system']['validate'] = 0;

            } elseif( $_GET['action2'] == 'create_loading' ){
                $data['system']['only_calculate'] = 0;
                $data['system']['validate'] = 0;
            }

            //print_r($data);
            $results = $econt_mysql->parcelImport($data);

            //Econt_mySQL::write_log($results);
            if ($results) {
                    if (!empty($results->result->e->error)) {
                        $result = array();
                        $result['warning'] = (string)$results->result->e->error;
                    } elseif (isset($results->result->e->loading_price->total)) {

                        $order_total_sum = number_format((float)$results->result->e->loading_price->total, 2, '.', '');
        //				echo $order_total_sum;
                        if( $_GET['action2'] == 'create_loading' ){

                        $result = array(
                            'order_id'    				=> $_GET['order_id'],
                            'loading_id'  				=> (string) $results->result->e->loading_id,
                            'loading_num' 				=> (string) $results->result->e->loading_num,
                            'pdf_url'     				=> (string) $results->result->e->pdf_url,
                            'total_shipping_cost' 		=> $order_total_sum,
                            'customer_shipping_cost'	=> $order_total_sum,
                            'currency_symbol'			=> $_GET['currency_symbol'],
                            'currency'					=> $_GET['currency'],
                        );

                        //$result['total_sum'] 		= $order_total_sum;
                        //$result['order_total_sum'] 	= $order_total_sum;

                        if($receiver_share_sum > 0){

                        $result['customer_shipping_cost'] 	= $receiver_share_sum;
                        }
                        if($free_shipping == 1 || $_GET['payment_side'] == 'SENDER'){ //da go testvam ?

                        $result['customer_shipping_cost'] 	= 0;

                        }

                        //print_r($result);
                        if (isset($results->pdf)) {

                            $result['blank_yes'] = (string) $results->pdf->blank_yes;
                            $result['blank_no'] = (string) $results->pdf->blank_no;

                        } else {

                            $result['blank_yes'] = '';
                            $result['blank_no'] = '';
                        }



                            $econt_mysql->addLoading($result);

                    $orders = new WC_Order($_GET['order_id']);

                    $shipping_items = $orders->get_items('shipping');

                    foreach ($shipping_items as $key => $value) {

                        // deprecated
                        //$shipping_method_id = $value['method_id'];
                        //$shipping_method_title = $value['name'];
                        //$shipping_item_id = $key;

                        // update
                        $item = new WC_Order_Item_Shipping( $key );
                        $item->set_method_title($value['name']);
                        $item->set_method_id($value['method_id']);
                        $item->set_total($result['customer_shipping_cost']);

                    }

                    //  deprecated
                    //	$update_shipping_args = array(
                    //      'method_id' => $shipping_method_id, 
                    //      'method_title' => $shipping_method_title, 
                    //      'cost' => $result['customer_shipping_cost'],
                    //	);
                    //            
                    //	if(method_exists($orders, 'update_shipping')){			
                    //      $orders->update_shipping($shipping_item_id, $update_shipping_args);			
                    //	}

                    update_post_meta($_GET['order_id'], 'Econt_Customer_Shipping_Cost', sanitize_text_field($result['customer_shipping_cost']));
                    update_post_meta($_GET['order_id'], 'Econt_Total_Shipping_Cost', sanitize_text_field($result['total_shipping_cost'] ));

                } elseif($_GET['action2'] == 'only_calculate_loading') {

                        $result = array(
                            'order_id'    				=> $_GET['order_id'],
                            'econt_shipping_expenses' 	=> array('total_shipping_cost' => $order_total_sum,
                                                                 'customer_shipping_cost' => $order_total_sum,
                                                                 'currency_symbol'	=> $_GET['currency_symbol'],
                                                            ),
                        );	

                        if($receiver_share_sum > 0){

                        $result = array(
                            'order_id'    					=> $_GET['order_id'],
                            'econt_shipping_expenses' 		=> array('total_shipping_cost' => $order_total_sum,
                                                                     'customer_shipping_cost' => $receiver_share_sum,
                                                                     'currency_symbol'	=> $_GET['currency_symbol'],
                                                                ),
                        );	

                        }

                        if($free_shipping == 1 || ($_GET['payment_side'] == 'SENDER' && $receiver_share_sum == 0)){
                        //else{

                        $result = array(
                            'order_id'    					=> $_GET['order_id'],
                            'econt_shipping_expenses' 		=>  array('total_shipping_cost' => $order_total_sum,
                                                                      'customer_shipping_cost' => __('free shipping', 'woocommerce-econt'),
                                                                      'currency_symbol'	=> '',
                                                                ),
                        );	

                        }
                        //set shipping cost session var for woocommerce_cart_calculate_fees at the bottom
                        if(!isset($_SESSION)) {
                            session_start();
                        }
                        $_SESSION['econt_shipping_cost'] = $result['econt_shipping_expenses']['customer_shipping_cost'];

                        }


                    }


            } else {
                $result['warning'] = __('error_connect', 'woocommerce-econt');
            }

          } //end of weight if

        }elseif($_GET['action2'] == 'delete_loading'){
            $econt_mysql->deleteLoading(array('loading_num' => $_GET['loading_num']));
            $result = array('status' => $_GET['loading_num']);
        }elseif($_GET['action2'] == 'shipping_method_change'){ //end of 'action2 == delete loading' if
            if(!isset($_SESSION)){ 
                session_start(); 
            } 
            $_SESSION['econt_shipping_cost'] = 0;
        }//end of 'action2 == shipping_method_change' if

        } //end of action2 if

        //$response = json_encode($result, JSON_UNESCAPED_UNICODE);
        $response = json_encode($result);

        echo $response;
        exit();


	}


	public function pluginname_ajaxurl() {
		?>
		<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		</script>
		<?php

	}

	private function getIfSet(&$value, $default = null){
    	return isset($value) ? $value : $default;
	}


}
new Econt_Ajax;

//add shipping cost to checkout total
add_action('woocommerce_cart_calculate_fees', 'woo_add_cart_fee');

function woo_add_cart_fee() {

	global $woocommerce;
	$woocommerce->shipping;
	$wc_econt = new WC_Econt_Shipping_Method;

	 if ( is_checkout() && (bool)$wc_econt->inc_shipping_cost == TRUE ) {
    	if(!isset($_SESSION)){ 
        	session_start(); 
    	} 
    	//write_log('session econt customer shipping cost:'.$_SESSION['econt_shipping_cost']);
    	$extracost = (isset($_SESSION['econt_shipping_cost']) ? $_SESSION['econt_shipping_cost'] : 0 );
    	WC()->cart->add_fee(__('Econt Express Shipping Method','woocommerce-econt'), $extracost);
	}
}

if ( ! function_exists('write_log')) {
   function write_log ( $log )  {
      if ( is_array( $log ) || is_object( $log ) ) {
         error_log( print_r( $log, true ) );
      } else {
         error_log( $log );
      }
   }
}

?>