
<p>
<a href="javascript:void(0);"  class="econt_office_locator button" id="office_locator" style="display:none;" title="<?php _e('office locator','woocommerce-econt') ?>"><?php _e('office locator','woocommerce-econt') ?></a>
</p>
<script type="text/javascript">
	function receiveMessage(event) {
		if (event.origin !== '<?php echo $office_locator_domain; ?>')
			return;

		message_array = event.data.split('||');
		getOfficeByOfficeCode(message_array[0]);
		jQuery.colorbox.close();
	}

	if (window.addEventListener) {
		window.addEventListener('message', receiveMessage, false);
	} else if (window.attachEvent) {
		window.attachEvent('onmessage', receiveMessage);
	}

	jQuery(document).ready(function() {
		
		if (jQuery('#econt_offices_town').val()) {
			url = '<?php echo $office_locator; ?>&address=' + jQuery('#econt_offices_town').val();
		} else {
			url = '<?php echo $office_locator; ?>';
		}

		jQuery('a#office_locator').colorbox({
			overlayClose: true,
			href : url,
			iframe : true,
			opacity: 0.5,
			width  : '1000',
			height : '700'
		});
		
		//jQuery('#econt_offices_town').change(function () {
		jQuery('#econt_offices_town').on('input',function(e){
			if (jQuery('#econt_offices_town').val()) {
				url = '<?php echo $office_locator; ?>&address=' + jQuery('#econt_offices_town').val();
			} else {
				url = '<?php echo $office_locator; ?>';
			}

			jQuery('a#office_locator').colorbox({
				overlayClose: true,
				href : url,
				iframe : true,
				opacity: 0.5,
				width  : '1000',
				height : '700'
			});
		});
		
		jQuery('#econt_offices_town').change(function () {
		//jQuery('#econt_offices_town').on('input',function(e){
			if (jQuery('#econt_offices_town').val()) {
				url = '<?php echo $office_locator; ?>&address=' + jQuery('#econt_offices_town').val();
			} else {
				url = '<?php echo $office_locator; ?>';
			}

			jQuery('a#office_locator').colorbox({
				overlayClose: true,
				href : url,
				iframe : true,
				opacity: 0.5,
				width  : '1000',
				height : '700'
			});
		});


	});
            
	function getOfficeByOfficeCode(office_code) {
		//alert(office_code);
		jQuery("#econt_offices").val(office_code);
		if (parseInt(office_code)) {
			jQuery.ajax({
				url: 'index.php?route=shipping/econt/getOfficeByOfficeCode',
				type: 'POST',
				data: 'office_code=' + parseInt(office_code),
				dataType: 'json',
				success: function(data) {
					if (!data.error) {
						jQuery('#office_city_id').val(data.city_id);
						html = '<option value="0"><?php _e('please select', 'woocommerce-econt') ?></option>';

						for (i = 0; i < data.offices.length; i++) {
							html += '<option ';
							if (data.offices[i]['office_id'] == data.office_id) {
								html += 'selected="selected"';
							}
							html += 'value="' + data.offices[i]['office_id'] + '">' + data.offices[i]['office_code'] + ', ' + data.offices[i]['name'] + ', ' + data.offices[i]['address'] +  '</option>';
						}

						jQuery('#office_id').html(html);
						jQuery('#office_code').val(office_code);
					}
				}
			});
		}
	}






</script>
            