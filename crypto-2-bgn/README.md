=== Crypto 2 BGN ===

Contributors: atanasantonov

Donation link: (LTC) LdoQwdNn2FQAap8QBYDP8H4TetGmhhx3vj

Tags: converter, coinmarketcap, crypto, currencies

Requires at least: 3.0.1

Tested up to: 4.9.5

Requires PHP: 5.6

Stable tag: trunk

License: GPLv2

License URI: http://www.gnu.org/licenses/gpl-2.0.html

Show specific cryptocurrency price in ulgarian Lev (BGN)
 
== Description ==
 
Plugin works coinmarketcap.com API.

Gets the price of specific cryptocurrency from coinmarketcap.com in EUR and convert it to BGN (Bulgarian Lev).

Has options for custom output.
  
== Installation ==

Download and activate the plugin. The plugin works with shortcode:

[crypto_to_bgn]

The deafult currency is Bitcoin (BTC)

With all parameters it will look like this

[crypto_to_bgn currency="bitcoin" equal="true" invert="false"]
 
== Frequently Asked Questions ==

= Can I choose another currency? =
 
Yes, you can just add parameter "currency" in the shortcode. So it will look like this [crypto_to_bgn currency="zcash"]

= Can I use just the rate? =
 
Yes, you can! Set parameter "equal" to false. So it will look like this [crypto_to_bgn currency="zcash" equal="false"]

= Can I invert the rate? =
 
Yes, just set the parameter "invert" to true. So it will look like this [crypto_to_bgn invert="true"]

= Can I use multiple shortcodes in my page? =
 
Yes, you can! Place as many shortodes as you want!

[crypto_to_bgn currency=bitcoin]

[crypto_to_bgn currency=ethereum]

[crypto_to_bgn currency=ripple]

= Can I use the plugin in a Widget? =
 
Yes, you can! 

Go to "Widgets" section choose "Text", add it to the desired location Sidebar for example.

Then place the shortcode in the content box and it's done!

= Can I use multiple shortcodes in a Widget? =
 
Yes, you can! You already know how.

== Donations ==

If you like the plugin and it helps you to offer better services at your site you can buy me a fruit!

(BTC) 1ATAnAsK2wwDGK6UKrUewJDWtZ6kgJXEYu
(LTC) LdoQwdNn2FQAap8QBYDP8H4TetGmhhx3vj
(ZEC) t1VXcnmXZPaEfXtkshmhBtWEHy1CHqETh1V

== Changelog ==
 
= 0.9 =
* First release.
* No changes.
 
== Upgrade Notice ==
 
= 0.9 =

No upgrades needed at this time.
