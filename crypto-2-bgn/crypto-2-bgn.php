<?php 
/*
Plugin Name: Crypto2BGN
Plugin URI:  https://github.com/atanasantonov/wordpress-plugins/tree/master/crypto-to-bgn
Description: Coinmarketcap.com based plugin for conversion of Cryptocurrencis to BGN
Version:     0.8
Author:      Atanas Antonov
Author URI:  http://nantstudio.bg/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: ns-crypto-to-bgn
Domain Path: /languages
*/

defined( 'ABSPATH' ) or die( 'I can\'t do anything alone! Sorry!' );

/*
Simple converter based on coinmarketcap.com exchange rates.
Some options for the output.
*/

add_shortcode('crypto2bgn', 'ns_crypto_to_bgn_convert');

function ns_crypto_to_bgn_convert($atts, $content = '') {
    
    ob_start();
    
    $params = shortcode_atts( array(
        'currency'  => 'bitcoin',
        'equal'     => 'true',
        'invert'    => 'false',
    ), $atts);
    
    try {
        
        // API's URI
        $url = 'https://api.coinmarketcap.com/v1/ticker/' . $params['currency'] . '/?convert=EUR';

        // API call
        $response = json_decode(wp_remote_retrieve_body(wp_remote_get($url)));
        
        // If is error return
        if( isset($response->error) ) {
            throw new Exception('ERROR: ' . strtoupper($response->error));
        }
        
        if( empty($response[0]->price_eur) ) {
            throw new Exception('ERROR: PRICE IN EURO NOT SET');
        }
        
        $price = $response[0]->price_eur;
        
        if( $params['equal'] !== 'true' ) {        
            $rate = $params['invert'] === 'false' ? (floatval($price) * 1.95583) : (1 / floatval($price) / 1.95583);
        } else {              
            if( empty($response[0]->symbol) ) {
                throw new Exception('ERROR: CURRENCY SYMBOL NOT SET');
            }

            if( $params['invert'] === 'false' ) {
                $rate = '1 ' . $response[0]->symbol . ' = ' . number_format(floatval($price) * 1.95583, "5", ".", " ") . ' BGN';
            } else {
                $rate = '1 BGN = ' . (1 / floatval($price) / 1.95583) . ' ' . $response[0]->symbol;
            }
        }     
            
    } catch ( Exception $e ) {        
        $rate = $e->getMessage();        
    }     
    
    echo $rate . "<br>";
        
    return ob_get_clean();

}


