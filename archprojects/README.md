The plugin is designed to help the making of buildings presentations. This includes information for the:

- project / projects
- building / buildings in the project
- floorplans in the bulding
- different types of spaces or objects on the floor
- detailed description of each space / flat - areas, rooms and other
- positioning the short text for each space on the floor
- drawing the boundaries to be highlighted of each space on the floor 

There is also capability of marking each space as sold/rented or free

Plugin is qTranslate compatible!

WIP - Adding a functionality to record contact information about the customers.

WIP - News section about the construction processes - date, photo and short description for particular event.
