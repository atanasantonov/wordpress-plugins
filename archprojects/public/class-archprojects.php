<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

if( !class_exists('Archprojects') ):
    
class Archprojects {
	
    private static $initiated = false;

    public static function init() {
        if ( !self::$initiated ) {
                self::init_hooks();
        }
    }

    /**
     * Initializes WordPress hooks
     */
    private static function init_hooks() {
        self::$initiated = true;
        
        load_plugin_textdomain( 'archprojects', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        
        add_shortcode('archprojects', 'projects');
//        add_action( 'the_post', array( 'Buildings', 'list_buildings' ));
    }
    
    public static function projects() {
        echo 'This is Buildings plugin!';
        return;
    }

}

endif;