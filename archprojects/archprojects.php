<?php
/**
 * @package Archprojects
 * @version 1.0
 */
/*
Plugin Name: Archprojects
Plugin URI: http://wordpress.org/plugins/archprojects/
Description: Powerful plugin to manage and present architectural projects.
Author: Xamarin Enduro
Text Domain: archprojects
Domain Path: /languages 
Version: 0.91
Author URI: http://atanas.be/
*/

// Exit if called directly
if (!defined( 'WPINC')) { die; }

if (!defined( 'ARCHPROJECTS_PLUGIN_DIR')) 
{
    define( 'ARCHPROJECTS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}

if (!defined( 'ARCHPROJECTS_ADMIN_ASSETS')) 
{
    define( 'ARCHPROJECTS_ADMIN_ASSETS', plugins_url('/admin/assets/', __FILE__));
}


if (!defined( 'ARCHPROJECTS_PLUGIN_VERSION')) 
{
    define( 'ARCHPROJECTS_PLUGIN_VERSION', '0.1' );
}

require_once( ARCHPROJECTS_PLUGIN_DIR . 'public/class-archprojects.php' );
add_action( 'init', array( 'Archprojects', 'init' ) );

if (is_admin()) {
    require_once( ARCHPROJECTS_PLUGIN_DIR . 'admin/class-archprojects-admin.php' );    
    add_action( 'init', array( 'Archprojects_Admin', 'init' ));
    
    // add the dropzone support
    require_once 'admin/core/includes/dropzone.php';    
    add_action( 'init', array( 'Dropzone', 'init' ) );
    
    // register query vars
    add_filter( 'query_vars', array('Archprojects_Admin', 'ap_add_query_vars' ));
        
    require_once 'admin/core/projects.php';    
    add_action( 'init', array( 'Projects', 'ap_projects' ) );
    add_action( 'save_post', array('Projects', 'ap_projects_save'));
    
    require_once 'admin/core/buildings.php';    
    add_action( 'init', array( 'AP_Buildings', 'buildings' ) );
    add_action( 'save_post', array('AP_Buildings', 'buildings_save'));
    
    require_once 'admin/core/floors.php';    
    add_action( 'init', array( 'AP_Floors', 'floors' ) );
    add_action( 'save_post', array('AP_Floors', 'floors_save'));
    
    require_once 'admin/core/objects.php';    
    add_action( 'init', array( 'AP_Objects', 'objects' ) );
    add_action( 'save_post', array('AP_Objects', 'objects_save'));
}