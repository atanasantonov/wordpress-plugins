<select class="ap-filter <?php echo $class; ?>" id="<?php echo $key; ?>" name="<?php echo $name; ?>">
    <?php if(sizeof($parents)<1) : ?>
    <option value=""><?php echo sprintf(__('No %s added', self::$text_domain), __($plural, self::$text_domain)); ?></option>    
    <?php else : ?>
    <option value=""><?php echo sprintf(__('Choose %s', self::$text_domain), __($singular, self::$text_domain)); ?></option>    
    <?php foreach ($parents as $parent) : ?>
<?php printf( '<option class="i18n-multilingual-display" value="%s"%s>%s</option>'."\n", esc_attr( $parent->ID ), selected( $parent->ID, $active, false ), $parent->post_title); ?>
<?php endforeach; ?>
<?php endif; ?>
</select>
