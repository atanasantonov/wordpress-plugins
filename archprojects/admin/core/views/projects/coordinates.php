<div class="row">
    <div class="col-xs-6">
        <label><?php echo __('Coordinates (Lat, Long)', 'archprojects'); ?></label>
        <input type="text" id="projects-coordinates" name="projects-coordinates" class="form-control" value="<?php echo get_post_meta($post_id->ID, "projects-coordinates", true); ?>">
    </div>
    <div class="col-xs-6">
        <label><?php echo __('API KEY', 'archprojects'); ?></label>
        <input type="text" id="google-api-key" name="google-api-key" class="form-control" value="<?php echo get_post_meta($post_id->ID, "google-api-key", true); ?>">
    </div>
</div>