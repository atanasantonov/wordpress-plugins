<div class="row">    
    <div class="col-md-2 col-sm-6 col-xs-12">
        <h4><?php _e( 'Location', 'archprojects' ); ?>:</h4>
    </div>
    
    <!-- Projects dropdown -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <label class="control-label"><?php _e('Project', 'archprojects'); ?></label>
        <?php $project = Archprojects_Admin::ap_parents_dropdown('projects', 'projects', 0, 'project', 'projects', $ancestors[2], 'form-control'); ?>
    </div>
    
    <!-- Buildings dropdown -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <label class="control-label"><?php _e('Building', 'archprojects'); ?></label>
        <?php $building = Archprojects_Admin::ap_parents_dropdown('buildings', 'buildings', $project, 'buildings', 'building', $ancestors[1], 'form-control'); ?>          
    </div>
    
    <!-- Floors dropdown -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <label class="control-label"><?php _e('Floor', 'archprojects'); ?></label>
        <?php $floors = Archprojects_Admin::ap_parents_dropdown('floors', 'post_parent', $building, 'floors', 'floor', $ancestors[0], 'form-control'); ?>        
    </div>
    
    <!-- Position on the floor -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <label class="control-label"><?php _e('Position on the floor', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-floor-position'; ?>
        <?php $value = (isset($meta[$key])) ? array_shift($meta[$key]) : ''; ?>
        <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php echo $value; ?>" class="form-control">
        <?php unset($key,$value); ?>
    </div>
    
    <!-- Highlight on the floor -->
    <div class="col-md-2 col-sm-6 col-xs-12">
        <label class="control-label"><?php _e('Highlight on the floor', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-image-map'; ?>
        <?php $value = (isset($meta[$key])) ? array_shift($meta[$key]) : ''; ?>
        <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php echo $value; ?>" class="form-control">
        <?php unset($key,$value); ?>
    </div>
</div>

<hr>

<div class="row">
        
    <div class="col-md-2 col-sm-6 col-xs-12">
        <h4><?php _e('Data', 'archprojects'); ?>:</h4>
    </div>

    <!-- object_type -->
    <div class="col-md-2 col-sm-6 col-xs-12"> 
        <label class="control-label"><?php _e('Type', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-type'; ?>
        <select id="<?php echo $key; ?>" name="<?php echo $key; ?>" class="form-control">
            <option value=""></option>
        <?php $types = get_posts(array( 'post_type'=> 'ap_object_types', 'orderby'=>'menu_order', 'order'=> 'ASC')); ?>
        <?php $active = (isset($meta[$key])) ? array_shift($meta[$key]) : ''; ?>
        <?php foreach ($types as $type) : ?>
            <?php printf( '<option class="i18n-multilingual-display" value="%s"%s>%s</option>'."\n", esc_attr( $type->ID ), selected( $type->ID, $active, false ), $type->post_title); ?>
        <?php endforeach; ?>
        </select>            
    </div>

    <!-- number -->
    <div class="col-md-2 col-sm-6 col-xs-12"> 
        <label class="control-label"><?php _e('Number', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-number'; ?>
        <?php $value = (isset($meta[$key])) ? array_shift($meta[$key]) : ''; ?>
        <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php echo $value; ?>" class="form-control">
        <?php unset($key,$value); ?>
    </div>

    <!-- area_clean --> 
    <div class="col-md-2 col-sm-6 col-xs-12"> 
        <label class="control-label"><?php _e('Area Clean', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-area-clean'; ?>
        <?php $value = (isset($meta[$key])) ? array_shift($meta[$key]) : '0.00'; ?>
        <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php echo $value; ?>" class="form-control">                
        <?php unset($key,$value); ?>
    </div>      

    <!-- area_total -->
    <div class="col-md-2 col-sm-6 col-xs-12"> 
        <label class="control-label"><?php _e('Area Total', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-area-total'; ?>
        <?php $value = (isset($meta[$key])) ? array_shift($meta[$key]) : '0.00'; ?>
        <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php echo $value; ?>" class="form-control">                
        <?php unset($key,$value); ?>   
    </div>

    <!-- price -->
    <div class="col-md-2 col-sm-6 col-xs-12"> 
        <label class="control-label"><?php _e('Price', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-price'; ?>
        <?php $value = (isset($meta[$key])) ? array_shift($meta[$key]) : ''; ?>
        <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php echo $value; ?>" class="form-control">                
        <?php unset($key,$value); ?>   
    </div>

</div><!-- .row 3 -->

<hr>

<div class="row">
    <div class="col-md-2 col-sm-6 col-xs-12">
        <h4><?php _e('Additional', 'archprojects'); ?>:</h4>
    </div>
    
    <!-- storage_type -->
    <div class="col-md-2 col-sm-6 col-xs-12"> 
        <label class="control-label"><?php _e('Type', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-add-type'; ?>
        <select id="<?php echo $key; ?>" name="<?php echo $key; ?>" class="form-control">
            <option value=""></option>
        <?php $types = get_posts(array( 'post_type'=> 'ap_object_add', 'orderby'=>'menu_order', 'order'=> 'ASC')); ?>
        <?php $active = (isset($meta[$key])) ? array_shift($meta[$key]) : ''; ?>
        <?php foreach ($types as $type) : ?>
            <?php printf( '<option class="i18n-multilingual-display" value="%s"%s>%s</option>'."\n", esc_attr( $type->ID ), selected( $type->ID, $active, false ), $type->post_title); ?>
        <?php endforeach; ?>
        </select>            
    </div>

    <!-- number -->
    <div class="col-md-2 col-sm-6 col-xs-12"> 
        <label class="control-label"><?php _e('Number', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-add-number'; ?>
        <?php $value = (isset($meta[$key])) ? array_shift($meta[$key]) : ''; ?>
        <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php echo $value; ?>" class="form-control">
        <?php unset($key,$value); ?>
    </div>

    <!-- area_clean --> 
    <div class="col-md-2 col-sm-6 col-xs-12"> 
        <label class="control-label"><?php _e('Area', 'archprojects'); ?></label>
        <?php $key = self::$slug.'-add-area'; ?>
        <?php $value = (isset($meta[$key])) ? array_shift($meta[$key]) : '0.00'; ?>
        <input type="text" id="<?php echo $key; ?>" name="<?php echo $key; ?>" value="<?php echo $value; ?>" class="form-control">                
        <?php unset($key,$value); ?>
    </div>  
    
</div>

<hr>

<div class="row">
    <div class="col-md-2 col-sm-6 col-xs-12">
        <h4><?php _e('Spaces', 'archprojects'); ?>:</h4>
    </div>
    
    <div class="col-md-10 col-sm-6 col-xs-12">
        <div class="row">
        <?php $key = self::$slug.'-spaces'; ?>    
        <?php $spaces = get_posts(array( 'post_type'=> 'ap_object_spaces', 'orderby'=>'post_title', 'order'=> 'ASC')); ?>    
        <?php $meta_spaces = (isset($meta[$key])) ? json_decode(array_shift($meta[$key])) : NULL; ?>
        <?php foreach ($spaces as $space) : ?>
            <?php $space_ID = $space->ID; ?>
            <?php $meta_data = (isset($meta_spaces->$space_ID)) ? $meta_spaces->$space_ID : ''; ?>
            <div class="col-md-2 col-sm-6 col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon i18n-multilingual-display"><?php echo $space->post_title; ?></span>
                    <input type="number" class="form-control" name="<?php echo $key; ?>[<?php echo $space->ID; ?>]" value="<?php echo $meta_data; ?>">
                </div>
            </div>   
        <?php endforeach; ?>
             
        </div>        
    </div>
</div>

<br>