<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

class Projects extends Archprojects_Admin {
    
    // add projects
    public static function ap_projects() { 
        
        __( 'Project', 'archprojects' );
        __( 'Projects', 'archprojects' );
        
        $labels = parent::ap_admin_make_labels( 'archprojects', 'archprojects', 'Project', 'Projects');

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'archprojects' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'archprojects',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'ap_projects' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
	);
        
        register_post_type('ap_projects', $args);
        add_action( 'do_meta_boxes', array( 'Projects', 'ap_projects_metaboxes' ), 10, 1);

    }
    
    public static function ap_projects_metaboxes($post_id) {
        
        add_meta_box( 
            'projects_coordinates',
            __('Google Maps', 'archprojects'),
             array( 'Projects', 'ap_projects_coordinates'),
            'ap_projects',
            'normal',
            'high',
            array($post_id)     
        );
    }

    public static function ap_projects_coordinates($post_id) {
        
        wp_nonce_field(basename(__FILE__), "coordinates-nonce");
        
        include_once 'views/projects/coordinates.php';
    }
    
    public static function ap_projects_save($post_id) {
        
        // check the nonce
        if (!isset($_POST["coordinates-nonce"]) || !wp_verify_nonce($_POST["coordinates-nonce"], basename(__FILE__)))
        return $post_id;
        
        if(!current_user_can("edit_post", $post_id))
        return $post_id;
        
        if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
        
        // If this isn't a 'ap_projects' post, don't update it.
        if ( get_post_type($post_id) != "ap_projects" ) return;
        
        if ( isset( $_POST['projects-coordinates'] ) ) {
            update_post_meta($post_id, "projects-coordinates", $_POST['projects-coordinates']);
        }
        
        if ( isset( $_POST['google-api-key'] ) ) {
            update_post_meta($post_id, "google-api-key", $_POST['google-api-key']);
        }
        
        
    }
    
}

