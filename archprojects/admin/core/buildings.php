<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

class AP_Buildings extends Archprojects_Admin {
    
    private static $class      = 'AP_Buildings';   
    private static $post_type  = 'ap_buildings';
    private static $parent     = 'ap_projects';
    private static $slug       = 'buildings';
    private static $prefix     = 'buildings_';
    
    public static $text_domain= 'archprojects';
    
    // later we loop the arrays for the save method
    private static $meta_boxes_nonces = array('parents', 'area', 'price');
    private static $meta_boxes_data = array('area', 'price');
    
    // add projects
    public static function buildings() { 
        
        __( 'Building', 'archprojects');
        __( 'Buildings', 'archprojects');
        __( 'Project', 'archprojects');
        
        $labels = parent::ap_admin_make_labels( 'archprojects', 'archprojects', 'Building', 'Buildings');

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', self::$text_domain ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'archprojects',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => self::$post_type ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'thumbnail' )
	);
        
        register_post_type(self::$post_type, $args);
        
        add_action( 'do_meta_boxes', array( self::$class, self::$prefix.'metaboxes' ), 10, 3);

    }
    
    // add metaboxes
    public static function buildings_metaboxes($post) {
        
        // <-- projects meta box
        $key = 'project';
        add_meta_box( 
            self::$prefix.$key,
            __('Parent Project', self::$text_domain),
            array( self::$class, self::$prefix.'parents'),
            self::$post_type,
            'side',
            'default',
            array($post)    
        );
        unset($key);
        // -->
        
        // <-- gallery meta box
        $key = 'gallery';
        add_meta_box( 
            self::$prefix.$key,
            __('Gallery', self::$text_domain),
             array( 'Archprojects_Admin', 'dropzone_html'),
            self::$post_type,
            'normal',
            'low',
            array('nonce' => wp_create_nonce("dropzone"), 'image-size'=>'large', 'meta-key'=>self::$slug.'-'.$key)     
        );
        unset($key);
        // -->
        
        // <-- area metabox 
        $key = 'area';
        add_meta_box( 
            self::$prefix.$key, 
            __('Area', self::$text_domain),
            array( self::$class, self::$prefix.$key),
            self::$post_type,
            'side',
            'low',
            array('nonce-name'=>$key.'-nonce', 'meta-key'=>self::$slug.'-'.$key)
        ); 
        unset($key);
        // -->     
        
        // <-- price metabox 
        $key = 'price';
        add_meta_box( 
            self::$prefix.$key, 
            __('Price', self::$text_domain),
            array( self::$class, self::$prefix.$key),
            self::$post_type,
            'side',
            'low',
            array('nonce-name'=>$key.'-nonce', 'meta-key'=>self::$slug.'-'.$key)
        ); 
        unset($key);
        // -->
        
    }
    
    public static function buildings_parents($post, $callback_args) {
        
        wp_nonce_field(basename(__FILE__), "parents-nonce"); 
        
        $parents = get_posts(
        array(
            'post_type'   => self::$parent, 
            'post_status' => 'publish',
            'orderby'     => 'title', 
            'order'       => 'ASC', 
            'numberposts' => -1 
            )
        );
        
        include_once 'views/templates/parent.php';
    }
    
    public static function buildings_area($post, $callback_args) {
        $args = $callback_args['args'];        
        wp_nonce_field(basename(__FILE__), $args['nonce-name']);
        $meta_key = $args['meta-key'];
        include 'views/templates/input.php';        
    }

    public static function buildings_price($post, $callback_args) {
        $args = $callback_args['args'];        
        wp_nonce_field(basename(__FILE__), $args['nonce-name']);
        $meta_key = $args['meta-key'];
        include 'views/templates/input.php';     
    }
    
    public static function buildings_save($post_id) { 
        
        if(!current_user_can("edit_post", $post_id))
        return $post_id;
        
        if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
        
        // If this isn't a 'ap_buildings' post, don't update it.
        if ( get_post_type($post_id) != self::$post_type ) return; 

        // check the nonces
        if(wp_is_post_revision($post_id))
        {
            foreach (self::$meta_boxes_nonces as $key) 
            {
                if (!isset($_POST[$key."-nonce"]) || !wp_verify_nonce($_POST[$key."-nonce"], basename(__FILE__)))
                echo $key."-nonce mismatch";
            }
        }
        
        // add post specific meta data
        foreach (self::$meta_boxes_data as $key) 
        {  
            if ( isset( $_POST[self::$slug.'-'.$key] ) ) 
            {
                update_post_meta($post_id, self::$slug.'-'.$key, $_POST[self::$slug.'-'.$key]);
            }
        } 
        
        // save the parent project
        if ( isset( $_POST['post_parent'] ) ) {
            $data = array(
                'ID' => $post_id,
                'post_parent' => $_POST['post_parent']
            );

            remove_action('save_post', array('AP_Buildings', 'buildings_save'));
            wp_update_post($data);
            add_action('save_post', array('AP_Buildings', 'buildings_save'));
        }
    }
    
}

