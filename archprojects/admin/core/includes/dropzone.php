<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

class Dropzone extends Archprojects_Admin {
    
    public static function init() {
        
        wp_enqueue_style( 'dropzonecss', ARCHPROJECTS_ADMIN_ASSETS.'css/dropzone.css', array(), ARCHPROJECTS_PLUGIN_VERSION);
        wp_enqueue_script( 'dropzonejs', ARCHPROJECTS_ADMIN_ASSETS.'js/dropzone.js', array(), ARCHPROJECTS_PLUGIN_VERSION);

        // register Dropzone functions
        add_action( 'wp_ajax_dropzone_existing', array( 'Dropzone', 'dropzone_existing' ));
        add_action( 'wp_ajax_dropzone_upload', array( 'Dropzone', 'dropzone_upload' ));
        add_action( 'wp_ajax_dropzone_delete', array( 'Dropzone', 'dropzone_delete' ));
        
        wp_register_script( 'dropzone-options', ARCHPROJECTS_ADMIN_ASSETS.'js/dropzone-options.js', array(), ARCHPROJECTS_PLUGIN_VERSION);
        wp_localize_script( 'dropzone-options', 'wp_options', array(
            'existing' => admin_url( 'admin-ajax.php?action=dropzone_existing' ),
            'read_file_error' => __( 'Read file error: ', 'archprojects' ), 
            'upload' => admin_url( 'admin-ajax.php?action=dropzone_upload' ),
            'delete' => admin_url( 'admin-ajax.php?action=dropzone_delete' ),                   
            'text_delete' => __( 'Delete', 'archprojects' ),
            'max_files_exceeded' => __( 'Max files exceeded!', 'archprojects' ), 
        ));  
        wp_enqueue_script( 'dropzone-options');
    }
    
    // init the Dropzone html
    public static function dropzone_html($post, $callback_args) {
        // sanitize the callback args
        $args = array_map( 'sanitize_text_field', $callback_args['args'] );
        
        // add data attributes for each arg
        $data = '';
        foreach ($args as $key => $value) {
            $data .=  ' data-'.$key.'="'.$value.'"';
        }
        
        // make the dropzone div
        $dz  = '<div id="dropzone" class="dropzone"></div>';         
        $dz .= '<input type="hidden" name="media-ids" value="">';
        $dz .= '<input type="hidden" id="dropzone-data" data-post-id="'.$post->ID.'" '.$data.'>';
        echo $dz;
        
        return;
    }
    
    // function to get existing images and emit them in the Dropzone
    public static function dropzone_existing() {

        $upload_dir = wp_upload_dir();            
        $post_id = sanitize_text_field($_POST['post_id']);            
        $meta_key = sanitize_text_field($_POST['meta_key']);
        $images = get_post_meta( $post_id, $meta_key);

        $result = array();
        if(is_array($images) && sizeof($images)>0)
        {
            foreach ($images as $image) {

                $path_to_file = $upload_dir['basedir'] . $image;
                if( $image != '' && file_exists($path_to_file))
                {
                  $obj['baseurl']  = $upload_dir['baseurl'];
                  $obj['name'] = $image;
                  $obj['size'] = filesize($path_to_file);
                  $result[] = $obj;
                }
                else
                {
                  $result['error'][] = '['.$url_to_file.'] | Файла не е открит!';
                  continue;
                }
            }
        }

        echo json_encode($result);
        wp_die();
    }

    // handle file uploads and add post meta
    public static function dropzone_upload() {
        
        // check the nonce
        if(!check_ajax_referer('dropzone', 'nonce')==='1')
        {
            $result['error'] = __( 'Invalid data', 'archprojects' );
            echo json_encode($result);
            wp_die();
        }
        
        // get the file name
        $filename = $_FILES['file']['name'];
        
        // get the parent post
        $post_id = sanitize_text_field($_POST['post-id']);
        $meta_key = sanitize_text_field($_POST['meta-key']);
        
        // get filetype
        $uploaded_file_type = wp_check_filetype(basename($filename));
        $file_type = $uploaded_file_type['type'];
        // allowed file types
        $allowed_file_types = array('image/jpg','image/jpeg','image/gif','image/png');
        // file type check
        if(!in_array($file_type, $allowed_file_types))
        {
            $result['error'] = __( 'Invalid file type', 'archprojects' );
            echo json_encode($result);
            wp_die();
        }
        
        // wp_handle_upload
        $handle = wp_handle_upload($_FILES['file'], array( 'test_form' => false ));
        
            $upload_dir = wp_upload_dir();
        
            if(isset($handle['error']))
            {
                $result['error'] = $handle['error'];
                echo json_encode($result);
                wp_die();
            }

            // resize the image
            $resized_image = image_make_intermediate_size( $upload_dir['path'] .'/'. basename($handle['file']), 640, 480 );
            if(!$resized_image)
            {
                $result['error'] =  __( 'Resize image error', 'archprojects' );
                echo json_encode($result);
                wp_die();
            }
            
            // insert the post meta
            $post_meta_id = add_post_meta( $post_id, $meta_key, $upload_dir['subdir'] .'/'. $resized_image['file'] );
            if(!$post_meta_id)
            {
                $result['error'] =  __( 'Add image to post error', 'archprojects' );
                echo json_encode($meta_key);
                wp_die();
            }
            
            // delete the original image
            if(file_exists($upload_dir['path'] .'/'. basename($handle['file'])))
            {
                unlink($upload_dir['path'] .'/'. basename($handle['file']));
            }
            
        echo json_encode($upload_dir['subdir'] .'/'. $resized_image['file']);
        wp_die();
    }
    
    // delete files and post meta
    public static function dropzone_delete() {
        
        // check the nonce
        if(!check_ajax_referer('dropzone', 'nonce')==='1')
        {
            $result['error'] = __( 'Invalid data', 'archprojects' );
            echo json_encode($result);
            wp_die();
        }
        
        // get the post data        
        if(delete_post_meta($_POST['post_id'], $_POST['meta_key'], $_POST['meta_value']))
        {
            $result['success'] = sprintf( __( 'Image [%s] deleted', 'archprojects' ), $_POST['meta_value']);
        }
        else
        {
            $result['error'] = sprintf( __( 'Error delete image [%s]', 'archprojects' ), $_POST['meta_value']);
        }
        
        echo json_encode($result);
        wp_die();
    }
    
}

