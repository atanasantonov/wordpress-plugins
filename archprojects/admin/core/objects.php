<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

class AP_Objects extends Archprojects_Admin {
    
    private static $class      = 'AP_Objects';   
    private static $post_type  = 'ap_objects';
    private static $parent     = FALSE;
    private static $slug       = 'objects';
    private static $prefix     = 'objects_';
    
    // later we loop the arrays for the save method
    private static $meta_boxes_nonces = array('data');
    private static $meta_boxes_data = array(
                                            'objects-floor-position', 
                                            'objects-image-map', 
                                            'objects-type', 
                                            'objects-number', 
                                            'objects-area-clean', 
                                            'objects-area-total',
                                            'objects-price',
                                            'objects-add-type',
                                            'objects-add-number',
                                            'objects-add-area',
                                            'objects-spaces',
                                        );
    
    // add projects
    public static function objects() {

        __( 'Object', 'archprojects');
        __( 'Objects', 'archprojects');
        
        // add filters to the dropdown
        if (isset($_GET['post_type']) && self::$post_type === $_GET['post_type'])
        {
            add_action('restrict_manage_posts', array( self::$class, 'restrict_'.self::$slug.'_by_parent' ));        
            add_action('pre_get_posts', array( self::$class, self::$slug.'_list' ));
            wp_dequeue_script( 'autosave' );
        }        
        
        add_action( 'wp_ajax_ap_objects_filters', array( self::$class, 'ap_objects_filters' )); 
        
        wp_register_script( 'ap_filters', ARCHPROJECTS_ADMIN_ASSETS.'js/ap_filters.js', array(), ARCHPROJECTS_PLUGIN_VERSION);        
        wp_localize_script( 'ap_filters', 'vars', array(
            'url' => admin_url( 'admin-ajax.php?action=ap_objects_filters'), 
        ));
        
        // Register Object post type 
        $labels = parent::ap_admin_make_labels( 'archprojects', 'archprojects', 'Object', 'Objects');
	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', self::$text_domain ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'archprojects',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => self::$post_type ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array('thumbnail')
	);        
        register_post_type(self::$post_type, $args);
        unset($labels,$args);
        
        // Register Object types post type 
        $labels = parent::ap_admin_make_labels( 'archprojects', 'archprojects', 'Object type', '- Object types');
	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', self::$text_domain ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'archprojects',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'ap_object_types' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'page-attributes' )
	);        
        register_post_type('ap_object_types', $args);
        unset($labels,$args);
        
        // Register Object spaces post type 
        $labels = parent::ap_admin_make_labels( 'archprojects', 'archprojects', 'Object space', '- Object spaces');
	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', self::$text_domain ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'archprojects',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'ap_object_spaces' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'page-attributes' )
	);        
        register_post_type('ap_object_spaces', $args);
        unset($labels,$args);
        
        // Register Object additional post type 
        $labels = parent::ap_admin_make_labels( 'archprojects', 'archprojects', 'Object additional', '- Object additionals');
	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', self::$text_domain ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'archprojects',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'ap_object_spaces' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'page-attributes' )
	);        
        register_post_type('ap_object_add', $args);
        unset($labels,$args);

        add_action( 'do_meta_boxes', array( self::$class, self::$prefix.'metaboxes' ));
    }  
    
    public static function restrict_objects_by_parent($post) {
        wp_enqueue_script( 'ap_filters');
                
        // Projects dropdown
        $ap_project = Archprojects_Admin::ap_parents_dropdown('projects', 'projects', 0, 'project', 'projects');        
        // Buildings dropdown
        $ap_building = Archprojects_Admin::ap_parents_dropdown('buildings', 'buildings', $ap_project, 'building', 'buildings');        
        // Floors dropdown
        Archprojects_Admin::ap_parents_dropdown('floors', 'floors', $ap_building, 'floor', 'floors');
    }
    
    public static function ap_objects_filters(){        
        if($_POST['post_type']==='projects')
        {
            $response['buildings'] = Archprojects_Admin::ap_parents_dropdown_set_options('buildings', $_POST['parent'], 'building', 'buildings');
            $response['floors'] = Archprojects_Admin::ap_parents_dropdown_set_options('floors', 0, 'floor', 'floors');
            echo json_encode($response);
            wp_die();
            return;
        }          
        if($_POST['post_type']==='buildings')
        {
            $response['floors'] = Archprojects_Admin::ap_parents_dropdown_set_options('floors', $_POST['parent'],  'floor', 'floors');
            echo json_encode($response);
            wp_die();
            return;
        }  
        $response[''] = Archprojects_Admin::ap_parents_dropdown_set_options('', 0);
        echo json_encode($response);
        wp_die();
        return;        
    }
    
    public static function objects_list($query){
        
        if ($query->is_search) {
            $query->set('post_parent', get_query_var( 'floor', 0 ));
        }        
    }
    
    // add metaboxes
    public static function objects_metaboxes($post) {        
        // <-- parents meta box
        $key = 'parents';
        add_meta_box( 
            self::$prefix.$key,
            __('Object Data', 'archprojects'),
            array( self::$class, self::$prefix.'data'),
            self::$post_type,
            'advanced',
            'high',
            array('nonce-name'=>$key.'-nonce')
        );
        unset($key);
        // --> 
    }
    
    public static function objects_data($post, $callback_args) {
        
        wp_enqueue_script( 'ap_filters');        
        wp_nonce_field(basename(__FILE__), "data-nonce");
        
        // Get previous post data if exists to fill the inputs
        $last_posts = get_posts(
            array(
                'post_type'   => self::$post_type,
                'orderby'     => 'ID', 
                'order'       => 'DESC', 
                'numberposts' => 1
                )
            );            
        $last_post = (sizeof($last_posts)>0) ? array_shift($last_posts) : NULL;
        
        // Object ancestors
        $ancestors = get_ancestors($post->ID, 'page');
        if(sizeof($ancestors)<1)
        {
            $ancestors = isset($last_post) ? get_ancestors($last_post->ID, 'page') : array(NULL, NULL, NULL);
        }        
        
        // Object meta
        $meta = get_post_meta($post->ID);
        if(sizeof($meta)<1)
        {
            $meta = isset($last_post) ? get_ancestors($last_post->ID, 'page') : array();
        }
        
        // Object spaces
        
        include_once 'views/objects/data.php';
    }
    
    public static function objects_area($post, $callback_args) {
        $args = $callback_args['args'];        
        wp_nonce_field(basename(__FILE__), $args['nonce-name']);
        $meta_key = $args['meta-key'];
        include 'views/templates/input.php';        
    }
    
    public static function objects_save($post_id) {
        
        if(!current_user_can("edit_post", $post_id))
        return $post_id;
        
        if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
        return $post_id;
        
        // If this isn't a 'ap_objects' post, don't update it.
        if ( get_post_type($post_id) != self::$post_type ) return; 

        // check the nonces
        if(wp_is_post_revision($post_id))
        {
            foreach (self::$meta_boxes_nonces as $key) 
            {
                if (!isset($_POST[$key."-nonce"]) || !wp_verify_nonce($_POST[$key."-nonce"], basename(__FILE__)))
                echo $key."-nonce mismatch";
            }
        }
        
        // save the parent project
        if ( isset( $_POST['post_parent'] ) ) {
            $data = array(
                'ID' => $post_id,
                'post_parent' => $_POST['post_parent']
            );

            remove_action('save_post', array(self::$class, self::$prefix.'save'));
            wp_update_post($data);
            add_action('save_post', array(self::$class, self::$prefix.'save'));
        }
        
        // add post specific meta data
        foreach (self::$meta_boxes_data as $key) 
        {  
            if ( isset( $_POST[$key] ) ) 
            {
                update_post_meta($post_id, $key, $_POST[$key]);
            }
        }
        
        // save the object spaces
        if ( isset( $_POST[self::$slug.'-spaces'] ) ) {
            update_post_meta($post_id, self::$slug.'-spaces', json_encode($_POST[self::$slug.'-spaces']));
        }
        
    }
    
}

