<?php

// Exit if called directly
if (!defined( 'WPINC')) { die; }

if( !class_exists('Archprojects_Admin') ):

class Archprojects_Admin {
	
    private static  $initiated   = false;
    public static   $lang;
    public static   $text_domain  = 'archprojects';

    public static function init() {
        if ( !self::$initiated ) {
                self::init_hooks();
        }
    }

    /**
     * Initializes WordPress hooks
     */
    private static function init_hooks() {
        
//        register_activation_hook( __FILE__, array( 'Archprojects_Admin', 'ap_plugin_activation' ) );
//        register_deactivation_hook( __FILE__, array( 'Archprojects_Admin', 'ap_plugin_deactivation' ) );
        
        self::$initiated = true;
        self::$lang = function_exists('qtranxf_getLanguage') ? qtranxf_getLanguage() : substr(get_locale(), 0, 2);
        
        load_plugin_textdomain( 'archprojects', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
        
        add_action( 'admin_menu', array( 'Archprojects_Admin', 'ap_admin_menu' )); 
        add_action( 'admin_init', array( 'Archprojects_Admin', 'ap_admin_init' ) );
        
        // register query vars
        add_filter( 'query_vars', array('Archprojects_Admin', 'ap_add_query_vars' ));
    }

    public static function ap_admin_init() {
        wp_enqueue_style( 'archprojects', plugins_url('/assets/css/archprojects.css', __FILE__), array(), ARCHPROJECTS_PLUGIN_VERSION);
  
        // list the phrases for the pomo parsers
        __( 'Add New', 'archprojects' );
        __( 'Add New %s', 'archprojects' );
        __( 'New %s', 'archprojects' );
        __( 'Edit %s', 'archprojects' );
        __( 'View %s', 'archprojects' );
        __( '%s', 'archprojects' ); //All
        __( 'Search %s', 'archprojects' );
        __( 'Belongs to', 'archprojects' );
        __( 'No %s found.', 'archprojects' );
        __( 'No %s found in Trash.', 'archprojects' );
    }
    
    public static function ap_admin_menu() {
        add_menu_page(__('Archprojects Dashboard', 'archprojects'), __('ArchProjects', 'archprojects'), 'activate_plugins', 'archprojects', array('Archprojects_Admin','ap_admin_dashboard'),'dashicons-admin-multisite');
        
        add_submenu_page( 
            'edit.php?post_type=ap_objects',
            'WPOrg Options',
            'WPOrg Options',
            'manage_options',
            'wporg',
            array( 'AP_Objects', 'objects' )
        );
        
        remove_menu_page('ap_projects');
    }
    
    public static function ap_admin_dashboard () {       
        return;
    }
    
    // init the dropzone
    public static function dropzone_html($post_id, $callback_args) {
        return Dropzone::dropzone_html($post_id, $callback_args);
    }
    
    public static function ap_admin_make_labels($plugin, $text_domain, $singular, $plural)
    {
        // assign the strings
        $singular_trans = __( $singular, $text_domain);
        $plural_trans = __( $plural, $text_domain);
        
        return array(
		'name'               => _x( $plural, 'post type general name', $text_domain ),
		'singular_name'      => _x( $singular, 'post type singular name', $text_domain ),
		'menu_name'          => _x( $plural, 'admin menu', $text_domain ),
		'name_admin_bar'     => _x( $singular, 'add new on admin bar', $text_domain ),
		'add_new'            => _x( 'Add New', mb_strtolower($singular), $text_domain ),
		'add_new_item'       => sprintf( __( 'Add New %s', $text_domain ), $singular_trans),
		'new_item'           => sprintf( __( 'New %s', $text_domain ), $singular_trans),
		'edit_item'          => sprintf( __( 'Edit %s', $text_domain ), $singular_trans),
		'view_item'          => sprintf( __( 'View %s', $text_domain ),$singular_trans),
		'all_items'          => sprintf( __( '%s', $text_domain ), $plural_trans),
		'search_items'       => sprintf( __( 'Search %s', $text_domain ), $plural_trans),
		'parent_item_colon'  => __( 'Belongs to', $text_domain ),
		'not_found'          => sprintf( __( 'No %s found.', $text_domain ), mb_strtolower($plural_trans)),
		'not_found_in_trash' => sprintf( __( 'No %s found in Trash.', $text_domain ), mb_strtolower($plural_trans))
	);
    }    
    
    public static function ap_add_query_vars($vars) {
        $vars[] = 'projects';
        $vars[] = 'buildings';
        $vars[] = 'floors';
        return $vars;
    }
    
    public static function ap_parents_dropdown($key, $name, $parent, $singular, $plural, $active=NULL, $class=NULL){
        $parents = get_posts(
            array(
                'post_type'   => 'ap_'.$key,
                'post_parent' => $parent,
                'post_status' => 'publish',
                'orderby'     => 'menu_order, title', 
                'order'       => 'ASC', 
                'numberposts' => -1 
                )
        ); 
        $active = (isset($active)) ? $active : get_query_var( $key, 0 );
        include 'core/views/templates/filters.php';
        return $active;
    }
    
    public static function ap_parents_dropdown_set_options($key, $parent, $singular, $plural){
        $parents = get_posts(
            array(
                'post_type'   => 'ap_'.$key,
                'post_parent' => $parent,
                'post_status' => 'publish',
                'orderby'     => 'menu_order, title', 
                'order'       => 'ASC', 
                'numberposts' => -1 
                )
        );
        
        if(sizeof($parents)<1)
        {
            $html = '<option value="">'.sprintf(__('No %s added', self::$text_domain), __($plural, self::$text_domain)).'</option>';
        }
        else 
        {
            $html = '<option value="">'.sprintf(__('Choose %s', self::$text_domain), __($singular, self::$text_domain)).'</option>';
            foreach ($parents as $parent)
            {
                $title = (function_exists('qtranxf_use')) ? qtranxf_use(self::$lang, $parent->post_title,false) : $parent->post_title;
                $html .= sprintf( '<option class="i18n-multilingual-display" value="%s"%s>%s</option>'."\n", esc_attr( $parent->ID ), selected( $parent->ID, get_query_var($post_type, 0), false ), $title);
            }
        }
        
        return $html;
    }
    
    public static function ap_dump($obj)
    {                
        echo '<pre>';
        var_dump($obj);
        echo '</pre>';
        return;
    }
    
    public static function ap_print($obj)
    {                
        echo '<pre>';
        print_r($obj);
        echo '</pre>';
        return;
    }
    
    public static function ap_plugin_activation() {
        echo '<h2>Plugin Archprojects activated successfuly!</h2>';
    }
    
    public static function ap_plugin_deactivation() {
        echo '<h2>Plugin Archprojects deactivated successfuly!</h2>';
    }
}

endif;