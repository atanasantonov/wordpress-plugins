// DROPZONE
Dropzone.options.dropzone = {
    
    autoDiscover: false,
    url: wp_options.upload,
    parallelUploads: 5,
    acceptedFiles: 'image/*',
    dictMaxFilesExceeded: wp_options.max_files_exceeded,
    addRemoveLinks: true,
    dictRemoveFile: wp_options.text_delete,
    
    init: function() {
        
        thisDropzone = this;
        
        thisDropzone.options.maxFiles = jQuery('#dropzone-data').data("maxFiles");
        
        var data = {
            post_id: jQuery('#dropzone-data').data("postId"),
            meta_key: jQuery('#dropzone-data').data("metaKey"),
        };
        
        jQuery.post( wp_options.existing, data, function(response) { 
            jQuery.each(JSON.parse(response), function(key,value){ 

                if( key === 'error' )
                {
                    jQuery(value).each(function(k,v){
                        jQuery('.images-errors').append('<div class="warning">' + wp_options.read_file_error + v + '</div>');
                    });
                }
                else
                {
                    var mockFile = { 
                        name: value.name, 
                        size: value.size
                    };
                    
                    thisDropzone.emit("addedfile", mockFile);
                    thisDropzone.emit("uploadprogress", mockFile, 100);
                    thisDropzone.createThumbnailFromUrl(mockFile, value.baseurl+value.name);
                    thisDropzone.files.push(mockFile);
                    thisDropzone.emit( "processing", mockFile);
                    thisDropzone.emit( "success", mockFile);
                    thisDropzone.emit( "complete", mockFile)
                }          
            });
            
        });
    },    
    
    sending: function(file, xhr, formData){
        formData.append("post-id", jQuery('#dropzone-data').data("postId"));
        formData.append("meta-key", jQuery('#dropzone-data').data("metaKey"));
        formData.append("image-size", jQuery('#dropzone-data').data("imageSize"));
        formData.append("nonce", jQuery('#dropzone-data').data("nonce"));
    },
    
    success: function (file, response) {

    },
    
    error: function (file, response) {
        file.previewElement.classList.add("dz-error");
    },
    
    removedfile: function(file) {
        
        jQuery.ajax({
            type: 'POST',
            url: wp_options.delete,
            data: {
                nonce: jQuery('#dropzone-data').data("nonce"),
                post_id: jQuery('#dropzone-data').data("postId"),
                meta_key: jQuery('#dropzone-data').data("metaKey"),
                meta_value: file.name,
            },
        });

        var _ref;
        return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;        
    }
  
};